function playBuffer(clip) {
    ctx = new AudioContext();
    buffer = ctx.createBuffer(2, clip.chunkSize, clip.sampleRate);
    var buf0    = buffer.getChannelData(0);
    var buf1    = buffer.getChannelData(1);
    for (i = 0; i < clip.chnkSize; ++i) {
        buf0[i] = 0;
        buf1[i] = 0;
    }

    clip.getChunk(buf0,buf1);
               
    node = ctx.createBufferSource(0);
    node.buffer = buffer;
    node.connect(ctx.destination);
    node.onended = function() {
        node.stop(0);
    }
    node.start();
}
