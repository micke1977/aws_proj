var node =null;
var buffer=null;
var ctx;
const CLIP_NAME_ENUM = {
    SimpleSineWaveSiren: 'SimpleSineWaveSiren',
    SimpleSineWaveSirenMultiple: 'SimpleSineWaveSirenMultiple',
    SimpleSineWaveSirenMultipleInput: 'SimpleSineWaveSirenMultipleInput',
}
const DIR = {
    UP: 'up',
    DOWN: 'down',
}
const CALC_VOL_TYPE = {
    ADSL: 'ADSL',
}

function init() {
    element = document.querySelector('.mainContainer');
    element.style.display = 'block';
    element1 = document.querySelector('.initContainer');
    element1.style.visibility = 'hidden';
    var i = 0;

    document.getElementById("fxBtnExp").style.display = 'none';
    document.getElementById("fxBtnColl").style.display = 'block';
    document.getElementById("fxRow").style.display = 'block';

    initFx2();
    initFx3();
}

function collapseFX() {
    document.getElementById("fxBtnExp").style.display = 'block';
    document.getElementById("fxBtnColl").style.display = 'none';
    document.getElementById("fxRow").style.display = 'none';
}
function expandFX() {
    document.getElementById("fxBtnExp").style.display = 'none';
    document.getElementById("fxBtnColl").style.display = 'block';
    document.getElementById("fxRow").style.display = 'block';
}
function mergeSnd(buf0,buf1,tempBuf0,tempBuf1) {
	for(var i=0;i<buf0.length;i++) {
		buf0[i]=buf0[i]+tempBuf0[i];
		buf1[i]=buf1[i]+tempBuf1[i];
	}
}
function isNotNull(value,exceptValue) {
    if(value==null || value =="") {
        return exceptValue;
    } else {
        return value;
    }
}

function drawAdsr() {
    var docu = document.getElementById("docu_adsr");
    docu.style.display = 'block';
    var canvas = document.getElementById("adsr-canvas");
    var ctx = canvas.getContext("2d");

    ctx.lineWidth = 10;
    ctx.strokeStyle = '#ffffff';

    ctx.moveTo(50, 120);
    ctx.lineTo(150, 20);
    ctx.stroke();
    
    ctx.moveTo(150, 20);
    ctx.lineTo(200, 70);
    ctx.stroke();

    ctx.moveTo(200, 70);
    ctx.lineTo(300, 70);
    ctx.stroke();

    ctx.moveTo(300, 70);
    ctx.lineTo(400, 120);
    ctx.stroke();

    ctx.moveTo(20, 130);
    ctx.lineTo(430, 130);
    ctx.stroke();

    ctx.font = '25px monospace';
    ctx.fillStyle = 'white';
    ctx.fillText('0', 50, 155); 

    ctx.fillText('1', 150, 155); 

    ctx.fillText('2', 200, 155); 

    ctx.fillText('3', 300, 155); 
    ctx.font = '15px monospace';
    ctx.fillText('0: Attack', 10, 195); 
    ctx.fillText('Start vol is the vol where the attack fades in from, usually 0, ', 15, 210); 
    ctx.fillText('Stop vol is the top vol where the attack ends, ', 15, 225); 
    ctx.fillText('Start nr is the starttime for the attack, ', 15, 240); 
    ctx.fillText('Stop nr is the endtime for the attack, ', 15, 255); 
    ctx.fillText('.... so example is that a long time and low vol-top gives a smooth attack, ', 15, 270); 
    ctx.fillText('and a short time with a high top gives a hard attack, drum as example ', 15, 285); 

    ctx.fillText('1: Decay', 10, 310); 
    ctx.fillText('The sound is decaying after the initial attack of the sound ', 15, 325); 

    ctx.fillText('2: Sustain', 10, 350); 
    ctx.fillText('Sustain is how long the sound is sustaining the volume ', 15, 365); 

    ctx.fillText('3: Release', 10, 390); 
    ctx.fillText('The part where the sound fades out, if it fades out suddenly or in a slow way.', 15, 405); 



    ctx.font = '30px monospace';
    ctx.fillText('CLOSE FRAME BY CLICKING INSIDE.', 10, 580); 

    
}
function closeCanvas() {
    var docu = document.getElementById("docu_adsr");
    docu.style.display = 'none';
}

function closeClown() {
    var docu = document.getElementById("clown");
    docu.style.display = 'none';
}

function drawClown() {
    new Audio('snd/scream.mp3').play();
    setTimeout(imgClown, 1000)
}
function imgClown() {
    var clown = document.getElementById("clown");
    clown.style.display = 'block';
}
function clip1() {
    new Audio('snd/clip1.wav').play();
}
function clip2() {
    new Audio('snd/clip2.wav').play();
}
function clip3() {
    new Audio('snd/clip3.wav').play();
}
function clipNote(oct,freq) {
    var clip;
    var freqCalculated =  0;

    if(freq == "c") {
        freqCalculated = 261.63
    }
    if(freq == "c#") {
        freqCalculated = 277.18
    }
    if(freq == "d") {
        freqCalculated = 293.66
    }
    if(freq == "d#") {
        freqCalculated = 311.13
    }
    if(freq == "e") {
        freqCalculated = 329.63
    }
    if(freq == "f") {
        freqCalculated = 349.23
    }
    if(freq == "f#") {
        freqCalculated = 369.99
    }
    if(freq == "g") {
        freqCalculated = 392.00
    }
    if(freq == "g#") {
        freqCalculated = 415.30
    }
    if(freq == "a") {
        freqCalculated = 440.00
    }
    if(freq == "a#") {
        freqCalculated = 466.16
    }
    if(freq == "b") {
        freqCalculated =493.88
    }
    

    if(oct == 1) {
        freqCalculated = freqCalculated * 2;
    }

    clip = new SimpleSineWaveSiren(freqCalculated);
    this.playBuffer(clip);
}

/*
C4	        261.63	131.87
C#4/Db4 	277.18	124.47
D4	        293.66	117.48
D#4/Eb4 	311.13	110.89
E4	        329.63	104.66
F4	        349.23	98.79
F#4/Gb4 	369.99	93.24
G4	        392.00	88.01
G#4/Ab4 	415.30	83.07
A4	        440.00	78.41
A#4/Bb4 	466.16	74.01
B4	        493.88	69.85
__60__
C5	        523.25	65.93
C#5/Db5 	554.37	62.23
D5	        587.33	58.74
D#5/Eb5 	622.25	55.44
E5	        659.25	52.33
F5	        698.46	49.39
F#5/Gb5 	739.99	46.62
G5	        783.99	44.01
G#5/Ab5 	830.61	41.54
A5	        880.00	39.20
A#5/Bb5 	932.33	37.00
B5	        987.77	34.93
__72__
C6	        1046.50	32.97
C#6/Db6 	1108.73	31.12
D6	        1174.66	29.37
D#6/Eb6 	1244.51	27.72
E6      	1318.51	26.17
F6	        1396.91	24.70
F#6/Gb6 	1479.98	23.31
G6      	1567.98	22.00
G#6/Ab6 	1661.22	20.77
A6      	1760.00	19.60
A#6/Bb6 	1864.66	18.50
B6      	1975.53	17.46
__84__
C7	        2093.00	16.48
C#7/Db7 	2217.46	15.56
D7	        2349.32	14.69
D#7/Eb7 	2489.02	13.86
E7	        2637.02	13.08
F7	        2793.83	12.35
F#7/Gb7 	2959.96	11.66
G7      	3135.96	11.00
G#7/Ab7 	3322.44	10.38
A7	        3520.00	9.80
A#7/Bb7 	3729.31	9.25
B7	        3951.07	8.73

*/

