divFx3 = null;
addClipButtonFx3 = null;
clipArrFx3 = [];
clipNrFx3 = 0;
divContent = null;

function initFx3() {
    
    divFx3 = document.getElementById("fx3");
    addClipButtonFx3 = document.createElement("button");
    addClipButtonFx3.innerHTML = "ADD NEW CLIP";
    addClipButtonFx3.classList.add('btnNotSelected');
    addClipButtonFx3.classList.add('mb2');
    addClipButtonFx3.addEventListener("click", addClipFx3);
    divFx3.appendChild(addClipButtonFx3);

    divContent = document.createElement("div");
    divFx3.appendChild(divContent);

    var playButtonFx3 = document.createElement("button");
    playButtonFx3.classList.add('btnNotSelected');
    playButtonFx3.addEventListener("click", function(){
        playFX3();
    });
    playButtonFx3.innerHTML = "PLAY CLIP";
    divFx3.appendChild(playButtonFx3);
}
function addClipFx3() {
    var clip = {};
    clip.clipNr = clipNrFx3;


    var divClipFx3 = document.createElement("div");
    divClipFx3.classList.add('row');
    divClipColFx3 = document.createElement("div")
    divClipColFx3.classList.add('col-md-12');
    divClipColFx3.setAttribute("id", "colFx3_"+clipNrFx3);
    divClipFx3.appendChild(divClipColFx3);


    var pClipNrFx3 = document.createElement("p");
    pClipNrFx3.innerHTML = 'CLIPNR:'+clipNrFx3;
    pClipNrFx3.classList.add("txtLabelL");
    divClipColFx3.appendChild(pClipNrFx3);

    var pWaveFormFx3 = document.createElement("p");
    pWaveFormFx3.innerHTML = 'WAVEFORM:';
    pWaveFormFx3.classList.add("inputLabelTxtHeadline");
    divClipColFx3.appendChild(pWaveFormFx3);

    
    var selectWaveFormFx3 = document.createElement("select");
    selectWaveFormFx3.setAttribute("id", "selectWaveFormFx3_"+clipNrFx3);
    selectWaveFormFx3.classList.add("dropdown")

    var waveFormOptionSineFx3 = document.createElement("option");
    waveFormOptionSineFx3.value = "Sine";
    waveFormOptionSineFx3.innerHTML = "Sine";
    selectWaveFormFx3.appendChild(waveFormOptionSineFx3);

    var waveFormOptionSawFx3 = document.createElement("option");
    waveFormOptionSawFx3.value = "Saw";
    waveFormOptionSawFx3.innerHTML = "Saw";
    selectWaveFormFx3.appendChild(waveFormOptionSawFx3);

    var waveFormOptionSquareFx3 = document.createElement("option");
    waveFormOptionSquareFx3.value = "Square";
    waveFormOptionSquareFx3.innerHTML = "Square";
    selectWaveFormFx3.appendChild(waveFormOptionSquareFx3);

    var waveFormOptionTriangleFx3 = document.createElement("option");
    waveFormOptionTriangleFx3.value = "Triangle";
    waveFormOptionTriangleFx3.innerHTML = "Triangle";
    selectWaveFormFx3.appendChild(waveFormOptionTriangleFx3);

    var waveFormOptionWhiteNoiseFx3 = document.createElement("option");
    waveFormOptionWhiteNoiseFx3.value = "White noise";
    waveFormOptionWhiteNoiseFx3.innerHTML = "White noise";
    selectWaveFormFx3.appendChild(waveFormOptionWhiteNoiseFx3);

    var waveFormOptionPinkNoiseFx3 = document.createElement("option");
    waveFormOptionPinkNoiseFx3.value = "Pink noise";
    waveFormOptionPinkNoiseFx3.innerHTML = "Pink noise";
    selectWaveFormFx3.appendChild(waveFormOptionPinkNoiseFx3);

    divClipColFx3.appendChild(selectWaveFormFx3);




    var divRowVolFreqFx3 = document.createElement("div");
    divRowVolFreqFx3.classList.add('row');
    divClipColFx3.appendChild(divRowVolFreqFx3);

    var divVolFx3 = document.createElement("div")
    divVolFx3.classList.add('col-md-6');
    divRowVolFreqFx3.appendChild(divVolFx3);

    var divInpLabelVolFx3 = document.createElement("div")
    divInpLabelVolFx3.classList.add('inputLabelTxt');
    divInpLabelVolFx3.innerHTML = 'VOL:';
    divVolFx3.appendChild(divInpLabelVolFx3);
    var spanVolFx3 = document.createElement("span")
    spanVolFx3.innerHTML = '0.50';
    spanVolFx3.setAttribute("id", "spanVolFx3_"+clipNrFx3);
    divInpLabelVolFx3.appendChild(spanVolFx3);

    var sliderVolFx3 = document.createElement("input");
    sliderVolFx3.type = 'range';
    clip.vol = 0.5;
    sliderVolFx3.value = 50;
    sliderVolFx3.min = 0;
    sliderVolFx3.max = 100;
    sliderVolFx3.setAttribute("id", "sliderVolFx3_"+clipNrFx3);
    sliderVolFx3.classList.add('slider');
    divVolFx3.appendChild(sliderVolFx3);

    sliderVolFx3.oninput = function() {
        var volStartFx3 = 0.00;
        var volStopFx3 = 1.00;
        var volDiffFx3 = Number(volStopFx3)-Number(volStartFx3);
        var volSliderStepFx3 = volDiffFx3/100;
        var volSliderValFx3 =(volSliderStepFx3*this.value).toFixed(2);
        var id = this.id.substring(13);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].vol = volSliderValFx3;
            }
        }
        document.getElementById("spanVolFx3_"+id).innerText = volSliderValFx3;
    }
    var divFreqFx3 = document.createElement("div")
    divFreqFx3.classList.add('col-md-6');
    divRowVolFreqFx3.appendChild(divFreqFx3);

    var divInpLabelFreqFx3 = document.createElement("div")
    divInpLabelFreqFx3.classList.add('inputLabelTxt');
    divInpLabelFreqFx3.innerHTML = 'FREQ:';
    divFreqFx3.appendChild(divInpLabelFreqFx3);
    var spanFreqFx3 = document.createElement("span")
    spanFreqFx3.innerHTML = (Number(27.5)+((Number(7040.00)-Number(27.50))/Number(2))).toFixed(2);
    spanFreqFx3.setAttribute("id", "spanFreqFx3_"+clipNrFx3);
    divInpLabelFreqFx3.appendChild(spanFreqFx3);

    var sliderFreqFx3 = document.createElement("input");
    sliderFreqFx3.type = 'range';
    clip.freq = (Number(27.5)+((Number(7040.00)-Number(27.50))/Number(2))).toFixed(2);
    sliderFreqFx3.value = 50;
    sliderFreqFx3.min = 0;
    sliderFreqFx3.max = 100;
    sliderFreqFx3.setAttribute("id", "sliderFreqFx3_"+clipNrFx3);
    sliderFreqFx3.classList.add('slider');
    divFreqFx3.appendChild(sliderFreqFx3);

    sliderFreqFx3.oninput = function() {
        var freqStartFx3 = 27.50;
        var freqStopFx3 = 7040.00;
        var freqDiffFx3 = Number(freqStopFx3)-Number(freqStartFx3);
        var freqSliderStepFx3 = freqDiffFx3/100;
        var freqSliderValFx3 =(freqSliderStepFx3*this.value).toFixed(2);
        var id = this.id.substring(14);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].freq = Number(freqStartFx3)+Number(freqSliderValFx3);
            }
        }
        document.getElementById("spanFreqFx3_"+id).innerText = Number(freqStartFx3)+Number(freqSliderValFx3);
    }


    //vol waving

    var labelWavingVolFx3 = document.createElement("div")
    labelWavingVolFx3.classList.add('inputLabelTxt');
    labelWavingVolFx3.innerHTML = 'VOL WAVING:';
    divClipColFx3.appendChild(labelWavingVolFx3);
    var divWavingVolFx3 = document.createElement("div")
    divWavingVolFx3.classList.add('borderDashed');
    divClipColFx3.appendChild(divWavingVolFx3);

    var activeImgWavingVolFx3 = document.createElement("img");
    divWavingVolFx3.appendChild(activeImgWavingVolFx3);
    activeImgWavingVolFx3.src = "img/checkboxFalse.png";
    activeImgWavingVolFx3.classList.add('checkbox');
    activeImgWavingVolFx3.setAttribute("id", "checkboxVolWaveFx3_"+clipNrFx3);
    activeImgWavingVolFx3.addEventListener("click", function(button){
        volWavingActiveFx3(button.target.id);
    });
    clip.volMoveActive = false;
    var activeSpanWavingVolFx3 = document.createElement("span");
    divWavingVolFx3.appendChild(activeSpanWavingVolFx3);
    activeSpanWavingVolFx3.classList.add('inputLabelTxt');
    activeSpanWavingVolFx3.innerHTML = ' ACTIVE';
    var divRowWavingVolFx3 = document.createElement("div");
    divRowWavingVolFx3.classList.add('row');
    divWavingVolFx3.appendChild(divRowWavingVolFx3);

    var divColSizeWavingVolFx3 = document.createElement("div");
    divColSizeWavingVolFx3.classList.add('col-md-6');
    inputSliderSizeWavingVolFx3(divColSizeWavingVolFx3,clip);
    divRowWavingVolFx3.appendChild(divColSizeWavingVolFx3);

    var divColStepWavingVolFx3 = document.createElement("div");
    divColStepWavingVolFx3.classList.add('col-md-6');
    inputSliderStepWavingVolFx3(divColStepWavingVolFx3,clip);
    divRowWavingVolFx3.appendChild(divColStepWavingVolFx3);

    //end vol waving

    //pitch waving
    
    var labelWavingPitchFx3 = document.createElement("div")
    labelWavingPitchFx3.classList.add('inputLabelTxt');
    labelWavingPitchFx3.innerHTML = 'PITCH WAVING:';
    divClipColFx3.appendChild(labelWavingPitchFx3);
    var divWavingPitchFx3 = document.createElement("div")
    divWavingPitchFx3.classList.add('borderDashed');
    divClipColFx3.appendChild(divWavingPitchFx3);

    var activeImgWavingPitchFx3 = document.createElement("img");
    divWavingPitchFx3.appendChild(activeImgWavingPitchFx3);
    activeImgWavingPitchFx3.src = "img/checkboxFalse.png";
    activeImgWavingPitchFx3.classList.add('checkbox');
    activeImgWavingPitchFx3.setAttribute("id", "checkboxPitchWaveFx3_"+clipNrFx3);
    activeImgWavingPitchFx3.addEventListener("click", function(button){
        pitchWavingActiveFx3(button.target.id);
    });
    clip.pitchMoveActive = false;
    var activeSpanWavingPitchFx3 = document.createElement("span");
    divWavingPitchFx3.appendChild(activeSpanWavingPitchFx3);
    activeSpanWavingPitchFx3.classList.add('inputLabelTxt');
    activeSpanWavingPitchFx3.innerHTML = ' ACTIVE';
    var divRowWavingPitchFx3 = document.createElement("div");
    divRowWavingPitchFx3.classList.add('row');
    divWavingPitchFx3.appendChild(divRowWavingPitchFx3);

    var divColSizeWavingPitchFx3 = document.createElement("div");
    divColSizeWavingPitchFx3.classList.add('col-md-6');
    inputSliderSizeWavingPitchFx3(divColSizeWavingPitchFx3,clip);
    divRowWavingPitchFx3.appendChild(divColSizeWavingPitchFx3);

    var divColStepWavingPitchFx3 = document.createElement("div");
    divColStepWavingPitchFx3.classList.add('col-md-6');
    inputSliderStepWavingPitchFx3(divColStepWavingPitchFx3,clip);
    divRowWavingPitchFx3.appendChild(divColStepWavingPitchFx3);

    //end pitch waving

    //pitch moving
    
    var labelMovingPitchFx3 = document.createElement("div")
    labelMovingPitchFx3.classList.add('inputLabelTxt');
    labelMovingPitchFx3.innerHTML = 'PITCH MOVING:';
    divClipColFx3.appendChild(labelMovingPitchFx3);
    var divMovingPitchFx3 = document.createElement("div")
    divMovingPitchFx3.classList.add('borderDashed');
    divClipColFx3.appendChild(divMovingPitchFx3);

    var activeImgMovingPitchFx3 = document.createElement("img");
    divMovingPitchFx3.appendChild(activeImgMovingPitchFx3);
    activeImgMovingPitchFx3.src = "img/checkboxFalse.png";
    activeImgMovingPitchFx3.classList.add('checkbox');
    activeImgMovingPitchFx3.setAttribute("id", "checkboxPitchMoveFx3_"+clipNrFx3);
    activeImgMovingPitchFx3.addEventListener("click", function(button){
        pitchMovingActiveFx3(button.target.id);
    });
    clip.pitchMoveSActive = false;
    var activeSpanMovingPitchFx3 = document.createElement("span");
    divMovingPitchFx3.appendChild(activeSpanMovingPitchFx3);
    activeSpanMovingPitchFx3.classList.add('inputLabelTxt');
    activeSpanMovingPitchFx3.innerHTML = ' ACTIVE';
    var divRowMovingPitchFx3 = document.createElement("div");
    divRowMovingPitchFx3.classList.add('row');
    divMovingPitchFx3.appendChild(divRowMovingPitchFx3);

    var divColSizeMovingPitchFx3 = document.createElement("div");
    divColSizeMovingPitchFx3.classList.add('col-md-6');
    inputSliderSizeMovingPitchFx3(divColSizeMovingPitchFx3,clip);
    divRowMovingPitchFx3.appendChild(divColSizeMovingPitchFx3);

    var divColStepMovingPitchFx3 = document.createElement("div");
    divColStepMovingPitchFx3.classList.add('col-md-6');
    inputSliderStepMovingPitchFx3(divColStepMovingPitchFx3,clip);
    divRowMovingPitchFx3.appendChild(divColStepMovingPitchFx3);




    var divColDirMovingPitchFx3 = document.createElement("div");
    divColDirMovingPitchFx3.classList.add('col-md-12');
    divRowMovingPitchFx3.appendChild(divColDirMovingPitchFx3);

    var pLabelDirMovePitchFx3 = document.createElement("p");
    pLabelDirMovePitchFx3.innerHTML = 'DIRECTION:';
    pLabelDirMovePitchFx3.classList.add("inputLabelTxtHeadline");
    divColDirMovingPitchFx3.appendChild(pLabelDirMovePitchFx3);
    
    
    
    var selectDirMovePitchFx3 = document.createElement("select");
    divColDirMovingPitchFx3.appendChild(selectDirMovePitchFx3);
    selectDirMovePitchFx3.setAttribute("id", "selectDirMovePitchFx3"+clipNrFx3);
    selectDirMovePitchFx3.classList.add("dropdown")
    
    var optionUpDirMovePitchFx3 = document.createElement("option");
    optionUpDirMovePitchFx3.value = "UP";
    optionUpDirMovePitchFx3.innerHTML = "UP";
    selectDirMovePitchFx3.appendChild(optionUpDirMovePitchFx3);

    var optionDownDirMovePitchFx3 = document.createElement("option");
    optionDownDirMovePitchFx3.value = "DOWN";
    optionDownDirMovePitchFx3.innerHTML = "DOWN";
    selectDirMovePitchFx3.appendChild(optionDownDirMovePitchFx3);
    //end pitch moving

    //ADSR
    var labelADSRFx3 = document.createElement("div")
    labelADSRFx3.classList.add('inputLabelTxt');
    labelADSRFx3.innerHTML = 'ADSR:';
    divClipColFx3.appendChild(labelADSRFx3);
    
    var divBDADSRFx3 = document.createElement("div")
    divBDADSRFx3.classList.add('borderDashed');
    divClipColFx3.appendChild(divBDADSRFx3);

    var divADSRFx3 = document.createElement("div")
    divADSRFx3.classList.add('row');
    divBDADSRFx3.appendChild(divADSRFx3);
    //_______label_attack___________________________________________
    var divColADSRAttackFx3 = document.createElement("div");
    divColADSRAttackFx3.classList.add('col-md-12');
    var pDivADSRAttackFx3 = document.createElement("p")
    pDivADSRAttackFx3.classList.add('inputLabelTxtHeadline');
    pDivADSRAttackFx3.innerHTML = "ATTACK";
    divColADSRAttackFx3.appendChild(pDivADSRAttackFx3);
    divADSRFx3.appendChild(divColADSRAttackFx3);
    //_____________attack_______________________________________________
    //--START_VOL
    var divColADSRAttackStartVolFx3 = document.createElement("div");
    divColADSRAttackStartVolFx3.classList.add('col-md-3');
    divColADSRAttackStartVolFx3.classList.add('inputLabelTxt');
    divColADSRAttackStartVolFx3.innerHTML="STARTVOL: ";

    var sliderAdsrAttackStartVolFx3 = document.createElement("input");
    sliderAdsrAttackStartVolFx3.type = 'text';
    sliderAdsrAttackStartVolFx3.id = 'adsrAttackStartVolValFx3_'+clipNrFx3;
    sliderAdsrAttackStartVolFx3.name = 'adsrAttackStartVolValFx3_'+clipNrFx3;
    sliderAdsrAttackStartVolFx3.classList.add('inpTxt');
    
    divColADSRAttackStartVolFx3.appendChild(sliderAdsrAttackStartVolFx3);
    divADSRFx3.appendChild(divColADSRAttackStartVolFx3);    

    //--STOP_VOL
    var divColADSRAttackStopVolFx3 = document.createElement("div");
    divColADSRAttackStopVolFx3.classList.add('col-md-3');
    divColADSRAttackStopVolFx3.classList.add('inputLabelTxt');
    divColADSRAttackStopVolFx3.innerHTML="STOPVOL: ";

    var sliderAdsrAttackStopVolFx3 = document.createElement("input");
    sliderAdsrAttackStopVolFx3.type = 'text';
    sliderAdsrAttackStopVolFx3.id = 'adsrAttackStopVolValFx3_'+clipNrFx3;
    sliderAdsrAttackStopVolFx3.name = 'adsrAttackStopVolValFx3_'+clipNrFx3;
    sliderAdsrAttackStopVolFx3.classList.add('inpTxt');
    
    divColADSRAttackStopVolFx3.appendChild(sliderAdsrAttackStopVolFx3);
    divADSRFx3.appendChild(divColADSRAttackStopVolFx3);    

    //---START_NR
    var divColADSRAttackStartNrFx3 = document.createElement("div");
    divColADSRAttackStartNrFx3.classList.add('col-md-3');
    divColADSRAttackStartNrFx3.classList.add('inputLabelTxt');
    divColADSRAttackStartNrFx3.innerHTML="STARTNR: ";

    var sliderAdsrAttackStartNrFx3 = document.createElement("input");
    sliderAdsrAttackStartNrFx3.type = 'text';
    sliderAdsrAttackStartNrFx3.id = 'adsrAttackStartNrValFx3_'+clipNrFx3;
    sliderAdsrAttackStartNrFx3.name = 'adsrAttackStartNrValFx3_'+clipNrFx3;
    sliderAdsrAttackStartNrFx3.classList.add('inpTxt');
    
    divColADSRAttackStartNrFx3.appendChild(sliderAdsrAttackStartNrFx3);
    divADSRFx3.appendChild(divColADSRAttackStartNrFx3);    

    //--STOP_NR
    var divColADSRAttackStopNrFx3 = document.createElement("div");
    divColADSRAttackStopNrFx3.classList.add('col-md-3');
    divColADSRAttackStopNrFx3.classList.add('inputLabelTxt');
    divColADSRAttackStopNrFx3.innerHTML="STOPNR: ";

    var sliderAdsrAttackStopNrFx3 = document.createElement("input");
    sliderAdsrAttackStopNrFx3.type = 'text';
    sliderAdsrAttackStopNrFx3.id = 'adsrAttackStopNrValFx3_'+clipNrFx3;
    sliderAdsrAttackStopNrFx3.name = 'adsrAttackStopNrValFx3_'+clipNrFx3;
    sliderAdsrAttackStopNrFx3.classList.add('inpTxt');
    
    divColADSRAttackStopNrFx3.appendChild(sliderAdsrAttackStopNrFx3);
    divADSRFx3.appendChild(divColADSRAttackStopNrFx3);    
     //---
    
     //_______label_decay___________________________________________
    var divColADSRDecayFx3 = document.createElement("div");
    divColADSRDecayFx3.classList.add('col-md-12');
    var pDivADSRDecayFx3 = document.createElement("p")
    pDivADSRDecayFx3.classList.add('inputLabelTxtHeadline');
    pDivADSRDecayFx3.innerHTML = "DECAY";
    divColADSRDecayFx3.appendChild(pDivADSRDecayFx3);
    divADSRFx3.appendChild(divColADSRDecayFx3);

    //_____DECAY________________________________________________________
    //--START_VOL
    var divColADSRDecayStartVolFx3 = document.createElement("div");
    divColADSRDecayStartVolFx3.classList.add('col-md-3');
    divColADSRDecayStartVolFx3.classList.add('inputLabelTxt');
    divColADSRDecayStartVolFx3.innerHTML="STARTVOL: ";

    var sliderAdsrDecayStartVolFx3 = document.createElement("input");
    sliderAdsrDecayStartVolFx3.type = 'text';
    sliderAdsrDecayStartVolFx3.id = 'adsrDecayStartVolValFx3_'+clipNrFx3;
    sliderAdsrDecayStartVolFx3.name = 'adsrDecayStartVolValFx3_'+clipNrFx3;
    sliderAdsrDecayStartVolFx3.classList.add('inpTxt');

    divColADSRDecayStartVolFx3.appendChild(sliderAdsrDecayStartVolFx3);
    divADSRFx3.appendChild(divColADSRDecayStartVolFx3);    

    //--STOP_VOL
    var divColADSRDecayStopVolFx3 = document.createElement("div");
    divColADSRDecayStopVolFx3.classList.add('col-md-3');
    divColADSRDecayStopVolFx3.classList.add('inputLabelTxt');
    divColADSRDecayStopVolFx3.innerHTML="STOPVOL: ";

    var sliderAdsrDecayStopVolFx3 = document.createElement("input");
    sliderAdsrDecayStopVolFx3.type = 'text';
    sliderAdsrDecayStopVolFx3.id = 'adsrDecayStopVolValFx3_'+clipNrFx3;
    sliderAdsrDecayStopVolFx3.name = 'adsDecayStopVolValFx3_'+clipNrFx3;
    sliderAdsrDecayStopVolFx3.classList.add('inpTxt');

    divColADSRDecayStopVolFx3.appendChild(sliderAdsrDecayStopVolFx3);
    divADSRFx3.appendChild(divColADSRDecayStopVolFx3);    

    //---START_NR
    var divColADSRDecayStartNrFx3 = document.createElement("div");
    divColADSRDecayStartNrFx3.classList.add('col-md-3');
    divColADSRDecayStartNrFx3.classList.add('inputLabelTxt');
    divColADSRDecayStartNrFx3.innerHTML="STARTNR: ";

    var sliderAdsrDecayStartNrFx3 = document.createElement("input");
    sliderAdsrDecayStartNrFx3.type = 'text';
    sliderAdsrDecayStartNrFx3.id = 'adsrDecayStartNrValFx3_'+clipNrFx3;
    sliderAdsrDecayStartNrFx3.name = 'adsrDecayStartNrValFx3_'+clipNrFx3;
    sliderAdsrDecayStartNrFx3.classList.add('inpTxt');

    divColADSRDecayStartNrFx3.appendChild(sliderAdsrDecayStartNrFx3);
    divADSRFx3.appendChild(divColADSRDecayStartNrFx3);    

    //--STOP_NR
    var divColADSRDecayStopNrFx3 = document.createElement("div");
    divColADSRDecayStopNrFx3.classList.add('col-md-3');
    divColADSRDecayStopNrFx3.classList.add('inputLabelTxt');
    divColADSRDecayStopNrFx3.innerHTML="STOPNR: ";

    var sliderAdsrDecayStopNrFx3 = document.createElement("input");
    sliderAdsrDecayStopNrFx3.type = 'text';
    sliderAdsrDecayStopNrFx3.id = 'adsrDecayStopNrValFx3_'+clipNrFx3;
    sliderAdsrDecayStopNrFx3.name = 'adsrDecayStopNrValFx3_'+clipNrFx3;
    sliderAdsrDecayStopNrFx3.classList.add('inpTxt');

    divColADSRDecayStopNrFx3.appendChild(sliderAdsrDecayStopNrFx3);
    divADSRFx3.appendChild(divColADSRDecayStopNrFx3);    
    //---

    //_______labelsustain___________________________________________
    var divColADSRSustainFx3 = document.createElement("div");
    divColADSRSustainFx3.classList.add('col-md-12');
    var pDivADSRSustainFx3 = document.createElement("p")
    pDivADSRSustainFx3.classList.add('inputLabelTxtHeadline');
    pDivADSRSustainFx3.innerHTML = "SUSTAIN";
    divColADSRSustainFx3.appendChild(pDivADSRSustainFx3);
    divADSRFx3.appendChild(divColADSRSustainFx3);
    //_____SUSTAIN________________________________________________________
    //--START_VOL
    var divColADSRSustainStartVolFx3 = document.createElement("div");
    divColADSRSustainStartVolFx3.classList.add('col-md-3');
    divColADSRSustainStartVolFx3.classList.add('inputLabelTxt');
    divColADSRSustainStartVolFx3.innerHTML="STARTVOL: ";

    var sliderAdsrSustainStartVolFx3 = document.createElement("input");
    sliderAdsrSustainStartVolFx3.type = 'text';
    sliderAdsrSustainStartVolFx3.id = 'adsrSustainStartVolValFx3_'+clipNrFx3;
    sliderAdsrSustainStartVolFx3.name = 'adsrSustainStartVolValFx3_'+clipNrFx3;
    sliderAdsrSustainStartVolFx3.classList.add('inpTxt');

    divColADSRSustainStartVolFx3.appendChild(sliderAdsrSustainStartVolFx3);
    divADSRFx3.appendChild(divColADSRSustainStartVolFx3);    

    //--STOP_VOL
    var divColADSRSustainStopVolFx3 = document.createElement("div");
    divColADSRSustainStopVolFx3.classList.add('col-md-3');
    divColADSRSustainStopVolFx3.classList.add('inputLabelTxt');
    divColADSRSustainStopVolFx3.innerHTML="STOPVOL: ";

    var sliderAdsrSustainStopVolFx3 = document.createElement("input");
    sliderAdsrSustainStopVolFx3.type = 'text';
    sliderAdsrSustainStopVolFx3.id = 'adsrSustainStopVolValFx3_'+clipNrFx3;
    sliderAdsrSustainStopVolFx3.name = 'adsrSustainStopVolValFx3_'+clipNrFx3;
    sliderAdsrSustainStopVolFx3.classList.add('inpTxt');

    divColADSRSustainStopVolFx3.appendChild(sliderAdsrSustainStopVolFx3);
    divADSRFx3.appendChild(divColADSRSustainStopVolFx3);    

    //---START_NR
    var divColADSRSustainStartNrFx3 = document.createElement("div");
    divColADSRSustainStartNrFx3.classList.add('col-md-3');
    divColADSRSustainStartNrFx3.classList.add('inputLabelTxt');
    divColADSRSustainStartNrFx3.innerHTML="STARTNR: ";

    var sliderAdsrSustainStartNrFx3 = document.createElement("input");
    sliderAdsrSustainStartNrFx3.type = 'text';
    sliderAdsrSustainStartNrFx3.id = 'adsrSustainStartNrValFx3_'+clipNrFx3;
    sliderAdsrSustainStartNrFx3.name = 'adsrSustainStartNrValFx3_'+clipNrFx3;
    sliderAdsrSustainStartNrFx3.classList.add('inpTxt');

    divColADSRSustainStartNrFx3.appendChild(sliderAdsrSustainStartNrFx3);
    divADSRFx3.appendChild(divColADSRSustainStartNrFx3);    

    //--STOP_NR
    var divColADSRSustainStopNrFx3 = document.createElement("div");
    divColADSRSustainStopNrFx3.classList.add('col-md-3');
    divColADSRSustainStopNrFx3.classList.add('inputLabelTxt');
    divColADSRSustainStopNrFx3.innerHTML="STOPNR: ";

    var sliderAdsrSustainStopNrFx3 = document.createElement("input");
    sliderAdsrSustainStopNrFx3.type = 'text';
    sliderAdsrSustainStopNrFx3.id = 'adsrSustainStopNrValFx3_'+clipNrFx3;
    sliderAdsrSustainStopNrFx3.name = 'adsrSustainStopNrValFx3_'+clipNrFx3;
    sliderAdsrSustainStopNrFx3.classList.add('inpTxt');

    divColADSRSustainStopNrFx3.appendChild(sliderAdsrSustainStopNrFx3);
    divADSRFx3.appendChild(divColADSRSustainStopNrFx3);    
    //---
    //_______labelRELEASE___________________________________________
    var divColADSRReleaseFx3 = document.createElement("div");
    divColADSRReleaseFx3.classList.add('col-md-12');
    var pDivADSRReleaseFx3 = document.createElement("p")
    pDivADSRReleaseFx3.classList.add('inputLabelTxtHeadline');
    pDivADSRReleaseFx3.innerHTML = "RELEASE";
    divColADSRReleaseFx3.appendChild(pDivADSRReleaseFx3);
    divADSRFx3.appendChild(divColADSRReleaseFx3);
    //_____RELEASE________________________________________________________
    //--START_VOL
    var divColADSRReleaseStartVolFx3 = document.createElement("div");
    divColADSRReleaseStartVolFx3.classList.add('col-md-3');
    divColADSRReleaseStartVolFx3.classList.add('inputLabelTxt');
    divColADSRReleaseStartVolFx3.innerHTML="STARTVOL: ";

    var sliderAdsrReleaseStartVolFx3 = document.createElement("input");
    sliderAdsrReleaseStartVolFx3.type = 'text';
    sliderAdsrReleaseStartVolFx3.id = 'adsrReleaseStartVolValFx3_'+clipNrFx3;
    sliderAdsrReleaseStartVolFx3.name = 'adsrReleaseStartVolValFx3_'+clipNrFx3;
    sliderAdsrReleaseStartVolFx3.classList.add('inpTxt');

    divColADSRReleaseStartVolFx3.appendChild(sliderAdsrReleaseStartVolFx3);
    divADSRFx3.appendChild(divColADSRReleaseStartVolFx3);    

    //--STOP_VOL
    var divColADSRReleaseStopVolFx3 = document.createElement("div");
    divColADSRReleaseStopVolFx3.classList.add('col-md-3');
    divColADSRReleaseStopVolFx3.classList.add('inputLabelTxt');
    divColADSRReleaseStopVolFx3.innerHTML="STOPVOL: ";

    var sliderAdsrReleaseStopVolFx3 = document.createElement("input");
    sliderAdsrReleaseStopVolFx3.type = 'text';
    sliderAdsrReleaseStopVolFx3.id = 'adsrReleaseStopVolValFx3_'+clipNrFx3;
    sliderAdsrReleaseStopVolFx3.name = 'adsrReleaseStopVolValFx3_'+clipNrFx3;
    sliderAdsrReleaseStopVolFx3.classList.add('inpTxt');

    divColADSRReleaseStopVolFx3.appendChild(sliderAdsrReleaseStopVolFx3);
    divADSRFx3.appendChild(divColADSRReleaseStopVolFx3);    

    //---START_NR
    var divColADSRReleaseStartNrFx3 = document.createElement("div");
    divColADSRReleaseStartNrFx3.classList.add('col-md-3');
    divColADSRReleaseStartNrFx3.classList.add('inputLabelTxt');
    divColADSRReleaseStartNrFx3.innerHTML="STARTNR: ";

    var sliderAdsrReleaseStartNrFx3 = document.createElement("input");
    sliderAdsrReleaseStartNrFx3.type = 'text';
    sliderAdsrReleaseStartNrFx3.id = 'adsrReleaseStartNrValFx3_'+clipNrFx3;
    sliderAdsrReleaseStartNrFx3.name = 'adsrReleaseStartNrValFx3_'+clipNrFx3;
    sliderAdsrReleaseStartNrFx3.classList.add('inpTxt');

    divColADSRReleaseStartNrFx3.appendChild(sliderAdsrReleaseStartNrFx3);
    divADSRFx3.appendChild(divColADSRReleaseStartNrFx3);    

    //--STOP_NR
    var divColADSRReleaseStopNrFx3 = document.createElement("div");
    divColADSRReleaseStopNrFx3.classList.add('col-md-3');
    divColADSRReleaseStopNrFx3.classList.add('inputLabelTxt');
    divColADSRReleaseStopNrFx3.innerHTML="STOPNR: ";

    var sliderAdsrReleaseStopNrFx3 = document.createElement("input");
    sliderAdsrReleaseStopNrFx3.type = 'text';
    sliderAdsrReleaseStopNrFx3.id = 'adsrReleaseStopNrValFx3_'+clipNrFx3;
    sliderAdsrReleaseStopNrFx3.name = 'adsrReleaseStopNrValFx3_'+clipNrFx3;
    sliderAdsrReleaseStopNrFx3.classList.add('inpTxt');

    divColADSRReleaseStopNrFx3.appendChild(sliderAdsrReleaseStopNrFx3);
    divADSRFx3.appendChild(divColADSRReleaseStopNrFx3);    

    //____________________________________________________________________________



    var removeClipButtonFx3 = document.createElement("button");
    removeClipButtonFx3.classList.add('btnNotSelected');
    removeClipButtonFx3.classList.add('mb2');
    removeClipButtonFx3.setAttribute("id", "removefx3_"+clipNrFx3);
    removeClipButtonFx3.addEventListener("click", function(button){
        removeFX3(button.target.id);
    });
    removeClipButtonFx3.innerHTML = "REMOVE CLIP";
    divClipColFx3.appendChild(removeClipButtonFx3);
    

    divContent.appendChild(divClipFx3);

    clipArrFx3.push(clip);
    clipNrFx3 ++;
}

function removeFX3(clipNr) {
    var id = clipNr.substring(10);
    for(var i = 0; i<clipArrFx3.length;i++) {
        if(clipArrFx3[i].clipNr == id) {
            clipArrFx3.splice(i,1);
            var element = document.getElementById("colFx3_"+id);
            element.parentNode.removeChild(element);
        }
    }
}
function volWavingActiveFx3(clipNr) {
    var id = clipNr.substring(19);
    for(var i = 0; i<clipArrFx3.length;i++) {
        if(clipArrFx3[i].clipNr == id) {
            clipArrFx3[i].volMoveActive = !clipArrFx3[i].volMoveActive;
            if(clipArrFx3[i].volMoveActive == false) {
                document.getElementById("checkboxVolWaveFx3_"+id).src="img/checkboxFalse.png";
            } else {
                document.getElementById("checkboxVolWaveFx3_"+id).src="img/checkboxTrue.png";
            }
        }
    }
}
function pitchWavingActiveFx3(clipNr) {
    var id = clipNr.substring(21);
    for(var i = 0; i<clipArrFx3.length;i++) {
        if(clipArrFx3[i].clipNr == id) {
            clipArrFx3[i].pitchMoveActive = !clipArrFx3[i].pitchMoveActive;
            if(clipArrFx3[i].pitchMoveActive == false) {
                document.getElementById("checkboxPitchWaveFx3_"+id).src="img/checkboxFalse.png";
            } else {
                document.getElementById("checkboxPitchWaveFx3_"+id).src="img/checkboxTrue.png";
            }
        }
    }
}

function pitchMovingActiveFx3(clipNr) {
    var id = clipNr.substring(21);
    for(var i = 0; i<clipArrFx3.length;i++) {
        if(clipArrFx3[i].clipNr == id) {
            clipArrFx3[i].pitchMoveSActive = !clipArrFx3[i].pitchMoveSActive;
            if(clipArrFx3[i].pitchMoveSActive == false) {
                document.getElementById("checkboxPitchMoveFx3_"+id).src="img/checkboxFalse.png";
            } else {
                document.getElementById("checkboxPitchMoveFx3_"+id).src="img/checkboxTrue.png";
            }
        }
    }
}

function inputSliderSizeWavingVolFx3(divColSizeWavingVolFx3,clip) {
    var divInpLabelSizeWavingVolFx3 = document.createElement("div")
    divInpLabelSizeWavingVolFx3.classList.add('inputLabelTxt');
    divInpLabelSizeWavingVolFx3.innerHTML = 'SIZE:';
    divColSizeWavingVolFx3.appendChild(divInpLabelSizeWavingVolFx3);
    var spanSizeWavingVolFx3 = document.createElement("span")
    spanSizeWavingVolFx3.innerHTML = '24.5';
    spanSizeWavingVolFx3.setAttribute("id", "spanSizeWavingVolFx3_"+clipNrFx3);
    divInpLabelSizeWavingVolFx3.appendChild(spanSizeWavingVolFx3);

    var sliderSizeWavingVolFx3 = document.createElement("input");
    sliderSizeWavingVolFx3.type = 'range';
    clip.volMoveMax = 24.5;
    clip.volMoveMin = -24.5;
    sliderSizeWavingVolFx3.value = 50;
    sliderSizeWavingVolFx3.min = 0;
    sliderSizeWavingVolFx3.max = 100;
    sliderSizeWavingVolFx3.setAttribute("id", "sliderSizeWavingVolFx3_"+clipNrFx3);
    sliderSizeWavingVolFx3.classList.add('slider');
    divColSizeWavingVolFx3.appendChild(sliderSizeWavingVolFx3);

    sliderSizeWavingVolFx3.oninput = function() {
        var volStartFx3 = 1.00;
        var volStopFx3 = 50.00;
        var volDiffFx3 = Number(volStopFx3)-Number(volStartFx3);
        var volSliderStepFx3 = volDiffFx3/100;
        var volSliderValFx3 =(volSliderStepFx3*this.value).toFixed(2);
        var id = this.id.substring(23);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].volMoveMax = volSliderValFx3;
                clipArrFx3[i].volMoveMin = -volSliderValFx3;
                document.getElementById("spanSizeWavingVolFx3_"+id).innerText = volSliderValFx3;
            }
        }
    }
}
function inputSliderStepWavingVolFx3(divColStepWavingVolFx3,clip) {
    var divInpLabelStepWavingVolFx3 = document.createElement("div")
    divInpLabelStepWavingVolFx3.classList.add('inputLabelTxt');
    divInpLabelStepWavingVolFx3.innerHTML = 'STEP:';
    divColStepWavingVolFx3.appendChild(divInpLabelStepWavingVolFx3);
    var spanStepWavingVolFx3 = document.createElement("span")
    spanStepWavingVolFx3.innerHTML = '2.49950';
    spanStepWavingVolFx3.setAttribute("id", "spanStepWavingVolFx3_"+clipNrFx3);
    divInpLabelStepWavingVolFx3.appendChild(spanStepWavingVolFx3);

    var sliderStepWavingVolFx3 = document.createElement("input");
    sliderStepWavingVolFx3.type = 'range';
    clip.volMoveStep = 2.49950;
    sliderStepWavingVolFx3.value = 50;
    sliderStepWavingVolFx3.min = 0;
    sliderStepWavingVolFx3.max = 100;
    sliderStepWavingVolFx3.setAttribute("id", "sliderStepWavingVolFx3_"+clipNrFx3);
    sliderStepWavingVolFx3.classList.add('slider');
    divColStepWavingVolFx3.appendChild(sliderStepWavingVolFx3);

    sliderStepWavingVolFx3.oninput = function() {
        var volStartFx3 = 0.00100;
        var volStopFx3 = 5.00000;
        var volDiffFx3 = Number(volStopFx3)-Number(volStartFx3);
        var volSliderStepFx3 = volDiffFx3/100;
        var volSliderValFx3 =(volSliderStepFx3*this.value).toFixed(5);
        var id = this.id.substring(23);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].volMoveStep = volSliderValFx3;
                document.getElementById("spanStepWavingVolFx3_"+id).innerText = volSliderValFx3;
            }
        }
    }
}

function inputSliderSizeWavingPitchFx3(divColSizeWavingPitchFx3,clip) {
    var divInpLabelSizeWavingPitchFx3 = document.createElement("div")
    divInpLabelSizeWavingPitchFx3.classList.add('inputLabelTxt');
    divInpLabelSizeWavingPitchFx3.innerHTML = 'SIZE:';
    divColSizeWavingPitchFx3.appendChild(divInpLabelSizeWavingPitchFx3);
    var spanSizeWavingPitchFx3 = document.createElement("span")
    spanSizeWavingPitchFx3.innerHTML = '24.5';
    spanSizeWavingPitchFx3.setAttribute("id", "spanSizeWavingPitchFx3_"+clipNrFx3);
    divInpLabelSizeWavingPitchFx3.appendChild(spanSizeWavingPitchFx3);

    var sliderSizeWavingPitchFx3 = document.createElement("input");
    sliderSizeWavingPitchFx3.type = 'range';
    clip.pitchMoveMax = 24.5;
    clip.pitchMoveMin = -24.5;
    sliderSizeWavingPitchFx3.value = 50;
    sliderSizeWavingPitchFx3.min = 0;
    sliderSizeWavingPitchFx3.max = 100;
    sliderSizeWavingPitchFx3.setAttribute("id", "sliderSizeWavingPitchFx3_"+clipNrFx3);
    sliderSizeWavingPitchFx3.classList.add('slider');
    divColSizeWavingPitchFx3.appendChild(sliderSizeWavingPitchFx3);

    sliderSizeWavingPitchFx3.oninput = function() {
        var pitchStartFx3 = 1.00;
        var pitchStopFx3 = 50.00;
        var pitchDiffFx3 = (pitchStopFx3-pitchStartFx3);
        var pitchSliderStepFx3 = pitchDiffFx3/100;
        var pitchSliderValFx3 =(pitchSliderStepFx3*this.value).toFixed(2);
        var id = this.id.substring(25);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].pitchMoveMax = pitchSliderValFx3;
                clipArrFx3[i].pitchMoveMin = -pitchSliderValFx3;
            }
        }
        document.getElementById("spanSizeWavingPitchFx3_"+id).innerText = pitchSliderValFx3;
    }
}
function inputSliderStepWavingPitchFx3(divColStepWavingPitchFx3,clip) {
    var divInpLabelStepWavingPitchFx3 = document.createElement("div")
    divInpLabelStepWavingPitchFx3.classList.add('inputLabelTxt');
    divInpLabelStepWavingPitchFx3.innerHTML = 'STEP:';
    divColStepWavingPitchFx3.appendChild(divInpLabelStepWavingPitchFx3);
    var spanStepWavingPitchFx3 = document.createElement("span")
    spanStepWavingPitchFx3.innerHTML = '2.49950';
    spanStepWavingPitchFx3.setAttribute("id", "spanStepWavingPitchFx3_"+clipNrFx3);
    divInpLabelStepWavingPitchFx3.appendChild(spanStepWavingPitchFx3);

    var sliderStepWavingPitchFx3 = document.createElement("input");
    sliderStepWavingPitchFx3.type = 'range';
    clip.pitchMoveStep = 2.49950;
    sliderStepWavingPitchFx3.value = 50;
    sliderStepWavingPitchFx3.min = 0;
    sliderStepWavingPitchFx3.max = 100;
    sliderStepWavingPitchFx3.setAttribute("id", "sliderStepWavingPitchFx3_"+clipNrFx3);
    sliderStepWavingPitchFx3.classList.add('slider');
    divColStepWavingPitchFx3.appendChild(sliderStepWavingPitchFx3);

    sliderStepWavingPitchFx3.oninput = function() {
        var pitchStartFx3 = 0.00100;
        var pitchStopFx3 = 5.00000;
        var pitchDiffFx3 = Number(pitchStopFx3)-Number(pitchStartFx3);
        var pitchSliderStepFx3 = pitchDiffFx3/100;
        var pitchSliderValFx3 =(pitchSliderStepFx3*this.value).toFixed(5);
        var id = this.id.substring(25);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].pitchMoveStep = pitchSliderValFx3;
            }
        }
        document.getElementById("spanStepWavingPitchFx3_"+id).innerText = pitchSliderValFx3;
    }
}


function inputSliderSizeMovingPitchFx3(divColSizeMovingPitchFx3,clip) {
    var divInpLabelSizeMovingPitchFx3 = document.createElement("div")
    divInpLabelSizeMovingPitchFx3.classList.add('inputLabelTxt');
    divInpLabelSizeMovingPitchFx3.innerHTML = 'SIZE:';
    divColSizeMovingPitchFx3.appendChild(divInpLabelSizeMovingPitchFx3);
    var spanSizeMovingPitchFx3 = document.createElement("span")
    spanSizeMovingPitchFx3.innerHTML = '24.5';
    spanSizeMovingPitchFx3.setAttribute("id", "spanSizeMovingPitchFx3_"+clipNrFx3);
    divInpLabelSizeMovingPitchFx3.appendChild(spanSizeMovingPitchFx3);

    var sliderSizeMovingPitchFx3 = document.createElement("input");
    sliderSizeMovingPitchFx3.type = 'range';
    clip.pitchMoveMax = 24.5;
    clip.pitchMoveMin = -24.5;
    sliderSizeMovingPitchFx3.value = 50;
    sliderSizeMovingPitchFx3.min = 0;
    sliderSizeMovingPitchFx3.max = 100;
    sliderSizeMovingPitchFx3.setAttribute("id", "sliderSizeMovingPitchFx3_"+clipNrFx3);
    sliderSizeMovingPitchFx3.classList.add('slider');
    divColSizeMovingPitchFx3.appendChild(sliderSizeMovingPitchFx3);

    sliderSizeMovingPitchFx3.oninput = function() {
        var pitchStartFx3 = 1.00;
        var pitchStopFx3 = 50.00;
        var pitchDiffFx3 = Number(pitchStopFx3)-Number(pitchStartFx3);
        var pitchSliderStepFx3 = pitchDiffFx3/100;
        var pitchSliderValFx3 =(pitchSliderStepFx3*this.value).toFixed(2);
        var id = this.id.substring(25);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].pitchMoveSMax = pitchSliderValFx3;
                clipArrFx3[i].pitchMoveSMin = -pitchSliderValFx3;
            }
        }
        document.getElementById("spanSizeMovingPitchFx3_"+id).innerText = pitchSliderValFx3;
    }
}
function inputSliderStepMovingPitchFx3(divColStepMovingPitchFx3,clip) {
    var divInpLabelStepMovingPitchFx3 = document.createElement("div")
    divInpLabelStepMovingPitchFx3.classList.add('inputLabelTxt');
    divInpLabelStepMovingPitchFx3.innerHTML = 'STEP:';
    divColStepMovingPitchFx3.appendChild(divInpLabelStepMovingPitchFx3);
    var spanStepMovingPitchFx3 = document.createElement("span")
    spanStepMovingPitchFx3.innerHTML = '2.49950';
    spanStepMovingPitchFx3.setAttribute("id", "spanStepMovingPitchFx3_"+clipNrFx3);
    divInpLabelStepMovingPitchFx3.appendChild(spanStepMovingPitchFx3);

    var sliderStepMovingPitchFx3 = document.createElement("input");
    sliderStepMovingPitchFx3.type = 'range';
    clip.pitchMoveSStep = 2.49950;
    sliderStepMovingPitchFx3.value = 50;
    sliderStepMovingPitchFx3.min = 0;
    sliderStepMovingPitchFx3.max = 100;
    sliderStepMovingPitchFx3.setAttribute("id", "sliderStepMovingPitchFx3_"+clipNrFx3);
    sliderStepMovingPitchFx3.classList.add('slider');
    divColStepMovingPitchFx3.appendChild(sliderStepMovingPitchFx3);

    sliderStepMovingPitchFx3.oninput = function() {
        var pitchStartFx3 = 0.00100;
        var pitchStopFx3 = 5.00000;
        var pitchDiffFx3 = Number(pitchStopFx3)-Number(pitchStartFx3);
        var pitchSliderStepFx3 = pitchDiffFx3/100;
        var pitchSliderValFx3 =(pitchSliderStepFx3*this.value).toFixed(5);
        var id = this.id.substring(25);
        for(var i = 0; i<clipArrFx3.length;i++) {
            if(clipArrFx3[i].clipNr == id) {
                clipArrFx3[i].pitchMoveSStep = pitchSliderValFx3;
            }
        }
        document.getElementById("spanStepMovingPitchFx3_"+id).innerText = pitchSliderValFx3;
    }
}

function playFX3() {
    var clip;
    var clipObj = {};
    var chunkSize = 0;
    validateObj = validateFx3();
    if(validateObj.isValid) {
        chunkSize = validateObj.chunkSize; 


        for(var i = 0;i<clipArrFx3.length;i++) {
            var id = clipArrFx3[i].clipNr;
            
            clipArrFx3[i].adsrAttackStartVol = document.getElementById("adsrAttackStartVolValFx3_"+id).value;
            clipArrFx3[i].adsrAttackStopVol = document.getElementById("adsrAttackStopVolValFx3_"+id).value;
            clipArrFx3[i].adsrAttackStarNr = document.getElementById("adsrAttackStartNrValFx3_"+id).value;
            clipArrFx3[i].adsrAttackStopNr = document.getElementById("adsrAttackStopNrValFx3_"+id).value;
    
            clipArrFx3[i].adsrDecayStartVol = document.getElementById("adsrDecayStartVolValFx3_"+id).value;
            clipArrFx3[i].adsrDecayStopVol = document.getElementById("adsrDecayStopVolValFx3_"+id).value;
            clipArrFx3[i].adsrDecayStarNr = document.getElementById("adsrDecayStartNrValFx3_"+id).value;
            clipArrFx3[i].adsrDecayStopNr = document.getElementById("adsrDecayStopNrValFx3_"+id).value;
    
            clipArrFx3[i].adsrSustainStartVol = document.getElementById("adsrSustainStartVolValFx3_"+id).value;
            clipArrFx3[i].adsrSustainStopVol = document.getElementById("adsrSustainStopVolValFx3_"+id).value;
            clipArrFx3[i].adsrSustainStarNr = document.getElementById("adsrSustainStartNrValFx3_"+id).value;
            clipArrFx3[i].adsrSustainStopNr = document.getElementById("adsrSustainStopNrValFx3_"+id).value;
    
            clipArrFx3[i].adsrReleaseStartVol = document.getElementById("adsrReleaseStartVolValFx3_"+id).value;
            clipArrFx3[i].adsreleaseStopVol = document.getElementById("adsrReleaseStopVolValFx3_"+id).value;
            clipArrFx3[i].adsreleaseStarNr = document.getElementById("adsrReleaseStartNrValFx3_"+id).value;
            clipArrFx3[i].adsreleaseStopNr = document.getElementById("adsrReleaseStopNrValFx3_"+id).value;
            clipArrFx3[i].pitchMoveSDirection = document.getElementById("selectDirMovePitchFx3"+id).value;
    
        }

        clipObj.chunkSize = chunkSize;
        clipObj.clipArr = clipArrFx3;   

        clip = new SimpleSineWaveSirenMultipleInput(clipObj);
        this.playBuffer(clip);
    } else {
        alert(clipObj.errorReason)
    }

    
}

function validateFx3() {
    isValid = true;
    chunkSize = -1;
    errorReason = '';
    for(var i = 0;i<clipArrFx3.length;i++) {
        clipNr = clipArrFx3[i].clipNr;
        timeArr = [];
        if(     document.getElementById("adsrAttackStartVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrAttackStopVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrAttackStartNrValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrAttackStopNrValFx3_"+clipNr).value != ""   &&
                
                document.getElementById("adsrDecayStartVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrDecayStopVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrDecayStartNrValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrDecayStopNrValFx3_"+clipNr).value != ""   &&
                
                document.getElementById("adsrSustainStartVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrSustainStopVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrSustainStartNrValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrSustainStopNrValFx3_"+clipNr).value != ""   &&
                
                document.getElementById("adsrReleaseStartVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrReleaseStopVolValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrReleaseStartNrValFx3_"+clipNr).value != ""   && 
                document.getElementById("adsrReleaseStopNrValFx3_"+clipNr).value != ""  
        ) {
            timeArr.push(document.getElementById("adsrAttackStartNrValFx3_"+clipNr).value);
            timeArr.push(document.getElementById("adsrAttackStopNrValFx3_"+clipNr).value);
            if(Number(chunkSize) < Number(document.getElementById("adsrAttackStopNrValFx3_"+clipNr).value)) {
                chunkSize = document.getElementById("adsrAttackStopNrValFx3_"+clipNr).value
            }
            timeArr.push(document.getElementById("adsrDecayStartNrValFx3_"+clipNr).value);
            timeArr.push(document.getElementById("adsrDecayStopNrValFx3_"+clipNr).value);
            if(Number(chunkSize) < Number(document.getElementById("adsrDecayStopNrValFx3_"+clipNr).value)) {
                chunkSize = document.getElementById("adsrDecayStopNrValFx3_"+clipNr).value
            }
            timeArr.push(document.getElementById("adsrSustainStartNrValFx3_"+clipNr).value);
            timeArr.push(document.getElementById("adsrSustainStopNrValFx3_"+clipNr).value);
            if(Number(chunkSize) < Number(document.getElementById("adsrSustainStopNrValFx3_"+clipNr).value)) {
                chunkSize = document.getElementById("adsrSustainStopNrValFx3_"+clipNr).value
            }
            timeArr.push(document.getElementById("adsrReleaseStartNrValFx3_"+clipNr).value);
            timeArr.push(document.getElementById("adsrReleaseStopNrValFx3_"+clipNr).value);
            if(Number(chunkSize) < Number(document.getElementById("adsrReleaseStopNrValFx3_"+clipNr).value)) {
                chunkSize = document.getElementById("adsrReleaseStopNrValFx3_"+clipNr).value
            }
            
        } else {
            isValid = false;
            errorReason = 'ADSR-section is missing values.';
        }

        for(var i = 0;i<timeArr.length;i++) {
            if(i==0) {
                //do nothing
            } else {
                if(Number(timeArr[i])<Number(timeArr[i-1])) {
                    isValid = false;
                    errorReason = 'The nr\'s in the ADSR is in wrong order.';
                }
            }
        }
    }

    return {isValid:isValid,chunkSize:chunkSize, errorReason:errorReason};
}