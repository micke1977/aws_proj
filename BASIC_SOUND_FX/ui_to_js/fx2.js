volFx2 = 0.5;
volSliderFx2 = null;

freqSliderFx2 = null;
freqSliderMinFx2 = 27.50;
freqSliderMaxFx2 = 7040.00;
freqSliderDiffFx2 = freqSliderMaxFx2-freqSliderMinFx2;
freqSliderDiffFx2100Part = freqSliderDiffFx2/100;
freqSliderFx2Val =(freqSliderDiffFx2100Part*50).toFixed(2);
//--
volMovingFx2Active = false;

volSliderMoveSizeFx2 = null;
volSliderMoveSizeMinFx2 = 1;
volSliderMoveSizeMaxFx2 = 50.0;
volSliderMoveSizeDiffFx2 = volSliderMoveSizeMaxFx2-volSliderMoveSizeMinFx2;
volSliderMoveSizeDiffFx2100Part = volSliderMoveSizeDiffFx2/100;
volSliderMoveSizeFx2Val = (volSliderMoveSizeDiffFx2100Part*50).toFixed(2);

volSliderMoveStepFx2 = null;
volSliderMoveStepMinFx2 = 0.001;
volSliderMoveStepMaxFx2 = 5;
volSliderMoveStepDiffFx2 = volSliderMoveStepMaxFx2-volSliderMoveStepMinFx2;
volSliderMoveStepDiffFx2100Part = volSliderMoveStepDiffFx2/100;
volSliderMoveStepFx2Val =(volSliderMoveStepDiffFx2100Part*50).toFixed(5);
//--
pitchSliderWaveFx2Active = false;

pitchSliderWaveSizeFx2 = null;
pitchSliderWaveSizeMinFx2 = 1;
pitchSliderWaveSizeMaxFx2 = 500.0;
pitchSliderWaveSizeDiffFx2 = pitchSliderWaveSizeMaxFx2-pitchSliderWaveSizeMinFx2;
pitchSliderWaveSizeDiffFx2100Part = pitchSliderWaveSizeDiffFx2/100;
pitchSliderWaveSizeFx2Val = (pitchSliderWaveSizeDiffFx2100Part*50).toFixed(2);

pitchSliderWaveStepFx2 = null;
pitchSliderWaveStepMinFx2 = 0.01;
pitchSliderWaveStepMaxFx2 = 10;
pitchSliderWaveStepDiffFx2 = pitchSliderWaveStepMaxFx2-pitchSliderWaveStepMinFx2;
pitchSliderWaveStepDiffFx2100Part = pitchSliderWaveStepDiffFx2/100;
pitchSliderWaveStepFx2Val =(pitchSliderWaveStepDiffFx2100Part*50).toFixed(5);
//--
pitchSliderMoveFx2Active = false;

pitchSliderMoveSizeFx2 = null;
pitchSliderMoveSizeMinFx2 = 1;
pitchSliderMoveSizeMaxFx2 = 500;
pitchSliderMoveSizeDiffFx2 = pitchSliderMoveSizeMaxFx2-pitchSliderMoveSizeMinFx2;
pitchSliderMoveSizeDiffFx2100Part = pitchSliderMoveSizeDiffFx2/100;
pitchSliderMoveSizeFx2Val = (pitchSliderMoveSizeDiffFx2100Part*50).toFixed(2);

pitchSliderMoveStepFx2 = null;
pitchSliderMoveStepMinFx2 = 0.01;
pitchSliderMoveStepMaxFx2 = 10;
pitchSliderMoveStepDiffFx2 = pitchSliderMoveStepMaxFx2-pitchSliderMoveStepMinFx2;
pitchSliderMoveStepDiffFx2100Part = pitchSliderMoveStepDiffFx2/100;
pitchSliderMoveStepFx2Val =(pitchSliderMoveStepDiffFx2100Part*50).toFixed(5);
//--
function initFx2() {
    document.getElementById("vol_fx2").innerText = volFx2;
    document.getElementById("freq_fx2").innerText = freqSliderFx2Val;

    document.getElementById("vol_move_size_fx2").innerText = volSliderMoveSizeFx2Val;
    document.getElementById("vol_move_step_fx2").innerText = volSliderMoveStepFx2Val;

    document.getElementById("pitch_wave_size_fx2").innerText = pitchSliderMoveSizeFx2Val;
    document.getElementById("pitch_wave_step_fx2").innerText = pitchSliderMoveStepFx2Val;

    document.getElementById("pitchMove_size_fx2").innerText = pitchSliderMoveSizeFx2Val;
    document.getElementById("pitchMove_step_fx2").innerText = pitchSliderMoveStepFx2Val;
    
    freqSliderFx2 = document.getElementById("freq_fx2_slider");
    freqSliderFx2.oninput = function() {
        freqSliderFx2Val = (freqSliderMinFx2+(this.value*freqSliderDiffFx2100Part)).toFixed(2);
        document.getElementById("freq_fx2").innerText = freqSliderFx2Val;
    }

    volSliderFx2 = document.getElementById("vol_fx2_slider");
    volSliderFx2.oninput = function() {
        volFx2 = (this.value/100).toFixed(2);
        document.getElementById("vol_fx2").innerText = volFx2;
    }

    volSliderMoveSizeFx2 = document.getElementById("vol_move_size_fx2_slider");
    volSliderMoveSizeFx2.oninput = function() {
        volSliderMoveSizeFx2Val = (volSliderMoveSizeMinFx2+(this.value*volSliderMoveSizeDiffFx2100Part)).toFixed(2);
        document.getElementById("vol_move_size_fx2").innerText = volSliderMoveSizeFx2Val;
    }
    volSliderMoveStepFx2 = document.getElementById("vol_move_step_fx2_slider");
    volSliderMoveStepFx2.oninput = function() {
        volSliderMoveStepFx2Val = (volSliderMoveStepMinFx2+(this.value*volSliderMoveStepDiffFx2100Part)).toFixed(5);
        document.getElementById("vol_move_step_fx2").innerText = volSliderMoveStepFx2Val;
    }

    pitchSliderWaveSizeFx2 = document.getElementById("pitch_wave_size_fx2_slider");
    pitchSliderWaveSizeFx2.oninput = function() {
        pitchSliderWaveSizeFx2Val = (pitchSliderWaveSizeMinFx2+(this.value*pitchSliderWaveSizeDiffFx2100Part)).toFixed(2);
        document.getElementById("pitch_wave_size_fx2").innerText = pitchSliderWaveSizeFx2Val;
    }
    pitchSliderWaveStepFx2 = document.getElementById("pitch_wave_step_fx2_slider");
    pitchSliderWaveStepFx2.oninput = function() {
        pitchSliderWaveStepFx2Val = (pitchSliderWaveStepMinFx2+(this.value*pitchSliderWaveStepDiffFx2100Part)).toFixed(5);
        document.getElementById("pitch_wave_step_fx2").innerText = pitchSliderWaveStepFx2Val;
    }

    pitchSliderMoveSizeFx2 = document.getElementById("pitchMove_size_fx2_slider");
    pitchSliderMoveSizeFx2.oninput = function() {
        pitchSliderMoveSizeFx2Val = (pitchSliderMoveSizeMinFx2+(this.value*pitchSliderMoveSizeDiffFx2100Part)).toFixed(2);
        document.getElementById("pitchMove_size_fx2").innerText = pitchSliderMoveSizeFx2Val;
    }
    pitchSliderMoveStepFx2 = document.getElementById("pitchMove_step_fx2_slider");
    pitchSliderMoveStepFx2.oninput = function() {
        pitchSliderMoveStepFx2Val = (pitchSliderMoveStepMinFx2+(this.value*pitchSliderMoveStepDiffFx2100Part)).toFixed(5);
        document.getElementById("pitchMove_step_fx2").innerText = pitchSliderMoveStepFx2Val;
    }

}
function volWavingActiveFx2() {
    volMovingFx2Active = !volMovingFx2Active;
    if(volMovingFx2Active == false) {
        document.getElementById("checkboxVolWaveFx2").src="img/checkboxFalse.png";
    } else {
        document.getElementById("checkboxVolWaveFx2").src="img/checkboxTrue.png";
    }
}
function pitchWavingActiveFx2() {
    pitchSliderWaveFx2Active = !pitchSliderWaveFx2Active;
    if(pitchSliderWaveFx2Active == false) {
        document.getElementById("checkboxPitchWaveFx2").src="img/checkboxFalse.png";
    } else {
        document.getElementById("checkboxPitchWaveFx2").src="img/checkboxTrue.png";
    }
}
function pitchMoveActiveFx2() {
    pitchSliderMoveFx2Active = !pitchSliderMoveFx2Active;
    if(pitchSliderMoveFx2Active == false) {
        document.getElementById("checkboxPitchMoveFx2").src="img/checkboxFalse.png";
    } else {
        document.getElementById("checkboxPitchMoveFx2").src="img/checkboxTrue.png";
    }
}

function validateFx2() {
    isValid = true;
    chunkSize = -1;
    errorReason = '';

    timeArr = [];
    if(     document.getElementById("adsr_attack_startVol_fx2").value != ""   && 
            document.getElementById("adsr_attack_stopVol_fx2").value != ""   && 
            document.getElementById("adsr_attack_startNr_fx2").value != ""   && 
            document.getElementById("adsr_attack_stopNr_fx2").value != ""   &&
            
            document.getElementById("adsr_decay_startVol_fx2").value != ""   && 
            document.getElementById("adsr_decay_stopVol_fx2").value != ""   && 
            document.getElementById("adsr_decay_startNr_fx2").value != ""   && 
            document.getElementById("adsr_decay_stopNr_fx2").value != ""   &&
            
            document.getElementById("adsr_sustain_startVol_fx2").value != ""   && 
            document.getElementById("adsr_sustain_stopVol_fx2").value != ""   && 
            document.getElementById("adsr_sustain_startNr_fx2").value != ""   && 
            document.getElementById("adsr_sustain_stopNr_fx2").value != ""   &&
            
            document.getElementById("adsr_release_startVol_fx2").value != ""   && 
            document.getElementById("adsr_release_stopVol_fx2").value != ""   && 
            document.getElementById("adsr_release_startNr_fx2").value != ""   && 
            document.getElementById("adsr_release_stopNr_fx2").value != ""   
    ) {
        timeArr.push(document.getElementById("adsr_attack_startNr_fx2").value);
        timeArr.push(document.getElementById("adsr_attack_stopNr_fx2").value);
        if(Number(chunkSize) < Number(document.getElementById("adsr_attack_stopNr_fx2").value)) {
            chunkSize = document.getElementById("adsr_attack_stopNr_fx2").value
        }
        timeArr.push(document.getElementById("adsr_decay_startNr_fx2").value);
        timeArr.push(document.getElementById("adsr_decay_stopNr_fx2").value);
        if(Number(chunkSize) < Number(document.getElementById("adsr_decay_stopNr_fx2").value)) {
            chunkSize = document.getElementById("adsr_decay_stopNr_fx2").value
        }
        timeArr.push(document.getElementById("adsr_sustain_startNr_fx2").value);
        timeArr.push(document.getElementById("adsr_sustain_stopNr_fx2").value);
        if(Number(chunkSize) < Number(document.getElementById("adsr_sustain_stopNr_fx2").value)) {
            chunkSize = document.getElementById("adsr_sustain_stopNr_fx2").value
        }
        timeArr.push(document.getElementById("adsr_release_startNr_fx2").value);
        timeArr.push(document.getElementById("adsr_release_stopNr_fx2").value);
        if(Number(chunkSize) < Number(document.getElementById("adsr_release_stopNr_fx2").value)) {
            chunkSize = document.getElementById("adsr_release_stopNr_fx2").value
        }
        
    } else {
        isValid = false;
        errorReason = 'ADSR-section is missing values.';
    }

    for(var i = 0;i<timeArr.length;i++) {
        if(i==0) {
            //do nothing
        } else {
            if(Number(timeArr[i])<Number(timeArr[i-1])) {
                isValid = false;
                errorReason = 'The nr\'s in the ADSR is in wrong order.';
            }
        }
    }

    return {isValid:isValid,chunkSize:chunkSize, errorReason:errorReason};
}
function createClipObjFx2(chunkSize) {
    var clipObj = {};

    var clipArr = [];

    var clipArrObj = {};

    clipArrObj.adsrAttackStartVol = document.getElementById("adsr_attack_startVol_fx2").value;
    clipArrObj.adsrAttackStopVol = document.getElementById("adsr_attack_stopVol_fx2").value;
    clipArrObj.adsrAttackStarNr = document.getElementById("adsr_attack_startNr_fx2").value;
    clipArrObj.adsrAttackStopNr = document.getElementById("adsr_attack_stopNr_fx2").value;
    
    clipArrObj.adsrDecayStartVol = document.getElementById("adsr_decay_startVol_fx2").value;
    clipArrObj.adsrDecayStopVol = document.getElementById("adsr_decay_stopVol_fx2").value;
    clipArrObj.adsrDecayStartNr = document.getElementById("adsr_decay_startNr_fx2").value;
    clipArrObj.adsrDecayStopNr = document.getElementById("adsr_decay_stopNr_fx2").value;
    
    clipArrObj.adsrSustainStartVol = document.getElementById("adsr_sustain_startVol_fx2").value;
    clipArrObj.adsrSustainStopVol = document.getElementById("adsr_sustain_stopVol_fx2").value;
    clipArrObj.adsrSustainStartNr = document.getElementById("adsr_sustain_startNr_fx2").value;
    clipArrObj.adsrSustainStopNr = document.getElementById("adsr_sustain_stopNr_fx2").value;
    
    clipArrObj.adsrReleaseStartVol = document.getElementById("adsr_release_startVol_fx2").value;
    clipArrObj.adsrReleaseStopVol = document.getElementById("adsr_release_stopVol_fx2").value;
    clipArrObj.adsrReleaseStarNr = document.getElementById("adsr_release_startNr_fx2").value;
    clipArrObj.adsrReleaseStopNr = document.getElementById("adsr_release_stopNr_fx2").value;

    clipArrObj.vol = volFx2;
    clipArrObj.freq = freqSliderFx2Val;

    clipArrObj.volMoveActive = volMovingFx2Active;
    clipArrObj.volMoveMax = volSliderMoveSizeFx2Val;
    clipArrObj.volMoveStep = volSliderMoveStepFx2Val;

    clipArrObj.pitchMoveActive = pitchSliderWaveFx2Active;
    clipArrObj.pitchMoveMin = -pitchSliderWaveSizeFx2Val;
    clipArrObj.pitchMoveMax = pitchSliderWaveSizeFx2Val;
    clipArrObj.pitchMoveStep = pitchSliderWaveStepFx2Val;
    
    clipArrObj.pitchMoveSActive = pitchSliderMoveFx2Active;
    clipArrObj.pitchMoveSDirection = fetcDirectionFx2();
    clipArrObj.pitchMoveSMax = pitchSliderMoveSizeFx2Val;
    clipArrObj.pitchMoveSStep = pitchSliderMoveStepFx2Val;

    clipArrObj.waveform = fetchWaveformFx2();

    clipArr.push(clipArrObj);

    clipObj.chunkSize = chunkSize;
    clipObj.clipArr = clipArr;

    return clipObj;
}
function fetchWaveformFx2() {
    return document.getElementById("waveformFx2").value;
}
function fetcDirectionFx2() {
    return document.getElementById("directionMovingPitchFx2").value;
}

function playFX2(clipName) {
    var clip;
    validateObj = validateFx2();
    if(validateObj.isValid) {
        var clipObj = createClipObjFx2(validateObj.chunkSize);
        clip = new SimpleSineWaveSirenMultipleInput(clipObj);
    } else {
        alert(clipObj.errorReason)
    }
    

    this.playBuffer(clip);
}