class SimpleSineWaveSiren extends AbstractSoundClip {
    vol = 0.0;
    freq = 0;
    volArr = [];
    freqArr = [];
    cyklePos = 0;
    lowPassFilterActive = null;
    pitch = 1.0;

    adsr_Attack_StarVol = 0.000;
    adsr_Attack_StopVol = 0.300;
    adsr_Attack_StartNr = 0;
    adsr_Attack_StoptNr = 2000;
    adsr_Attack_VolStep = (this.adsr_Attack_StopVol-this.adsr_Attack_StarVol) / (this.adsr_Attack_StoptNr-this.adsr_Attack_StartNr);

    adsr_Decay_StarVol = 0.300;
    adsr_Decay_StopVol = 0.100;
    adsr_Decay_StartNr = 2000;
    adsr_Decay_StoptNr = 5000;
    adsr_Decay_VolStep = (this.adsr_Decay_StopVol-this.adsr_Decay_StarVol) / (this.adsr_Decay_StoptNr-this.adsr_Decay_StartNr);

    adsr_Sustain_StarVol = 0.100;
    adsr_Sustain_StopVol = 0.050;
    adsr_Sustain_StartNr = 5000;
    adsr_Sustain_StoptNr = 10000;
    adsr_Sustain_VolStep = (this.adsr_Sustain_StopVol-this.adsr_Sustain_StarVol) / (this.adsr_Sustain_StoptNr-this.adsr_Sustain_StartNr);

    adsr_Release_StarVol = 0.050;
    adsr_Release_StopVol = 0.000;
    adsr_Release_StartNr = 10000;
    adsr_Release_StoptNr = 16000;
    adsr_Release_VolStep = (this.adsr_Release_StopVol-this.adsr_ReleaseStarVol) / (this.adsr_Release_StoptNr-this.adsr_Release_StartNr);



	constructor(freq) {
		super();
        this.chunkSize = 16000;

        this.freq=freq;

    }
    getChunk(buf0,buf1) {
        this.calcVolAndFreq();
        for(var i = 0;i<this.chunkSize;i++) {
            var obj =Waves.getSine(i,
                this.freqArr[i],this.volArr[i],
                this.cyklePos,buf0,buf1,
                this.pitch,this.sampleRate,
                this.lowPassFilterActive,this.sampleSize,
                this.balL,this.balR
            ); 
            this.cyklePos = obj.cyklePos;

        }
    }
    calcVolAndFreq() {
        for(var i = 0;i<this.chunkSize;i++) {
            if(i>= this.adsr_Attack_StartNr && i<this.adsr_Attack_StoptNr) {
                this.vol += this.adsr_Attack_VolStep;
                this.volArr[i] = this.vol;
            }
            else if(i>= this.adsr_Decay_StartNr && i<this.adsr_Decay_StoptNr) {
                this.vol += this.adsr_Decay_VolStep;
                this.volArr[i] = this.vol;
            }
            else if(i>= this.adsr_Sustain_StartNr && i<this.adsr_Sustain_StoptNr) {
                this.vol += this.adsr_Sustain_VolStep;
                this.volArr[i] = this.vol;
            }
            else if(i>= this.adsr_Release_StartNr && i<this.adsr_Release_StoptNr) {
                this.vol += this.adsr_Release_VolStep;
                this.volArr[i] = this.vol;
            }
            if(this.volArr[i] < 0.000) {
                this.volArr[i] = 0.000
            }
            this.volArr[i] = this.volArr[i].toFixed(7);


        }


        for(var i = 0;i<this.chunkSize;i++) {
            this.freqArr[i] = this.freq;
        }
    }
} 