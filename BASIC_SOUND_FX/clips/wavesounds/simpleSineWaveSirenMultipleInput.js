class SimpleSineWaveSirenMultipleInput extends AbstractSoundClip {
    clipArr = [];
    nrOfClips = -1;

	constructor(inValue) {
		super();
        this.chunkSize = inValue.chunkSize;
        this.init(inValue);
    }
    init(inValue) {
//--
        for(var arrNr = 0;arrNr<inValue.clipArr.length;arrNr++) {

            this.clipArr.push({})

            this.clipArr[arrNr].vol = 0;
            this.clipArr[arrNr].mainVol = isNotNull(inValue.clipArr[arrNr].vol,1.0);
            this.clipArr[arrNr].balL = isNotNull(inValue.clipArr[arrNr].balL,1.0);
            this.clipArr[arrNr].balR = isNotNull(inValue.clipArr[arrNr].balR,1.0);
            this.clipArr[arrNr].freq = isNotNull(inValue.clipArr[arrNr].freq,440.00);
            this.clipArr[arrNr].volArr = [];
            this.clipArr[arrNr].freqArr = [];
            this.clipArr[arrNr].cyklePos = 0;
            this.clipArr[arrNr].lowPassFilterActive = isNotNull(inValue.clipArr[arrNr].lpfActive,false);
            this.clipArr[arrNr].pitch = isNotNull(inValue.clipArr[arrNr].pitch,1.0);

            this.clipArr[arrNr].volMove = isNotNull(inValue.clipArr[arrNr].volMoveActive,false);
            this.clipArr[arrNr].volMoveIndex = 1.0;
            this.clipArr[arrNr].volMoveDirection = isNotNull(inValue.clipArr[arrNr].volMoveDirection,DIR.UP);
            this.clipArr[arrNr].volMoveMax = isNotNull(inValue.clipArr[arrNr].volMoveMax,25);
            this.clipArr[arrNr].volMoveMin = isNotNull(inValue.clipArr[arrNr].volMoveMin,-25);
            this.clipArr[arrNr].volMoveDist = isNotNull(inValue.clipArr[arrNr].volMoveStep,0.01);

            this.clipArr[arrNr].pitchMove = isNotNull(inValue.clipArr[arrNr].pitchMoveActive,false);
            this.clipArr[arrNr].pitchMoveIndex = 1.0;
            this.clipArr[arrNr].pitchMoveDirection = isNotNull(inValue.clipArr[arrNr].pitchMoveDirection,DIR.UP);
            this.clipArr[arrNr].pitchMoveMax = isNotNull(inValue.clipArr[arrNr].pitchMoveMax,250);
            this.clipArr[arrNr].pitchMoveMin = isNotNull(inValue.clipArr[arrNr].pitchMoveMin,-250);
            this.clipArr[arrNr].pitchMoveDist = isNotNull(inValue.clipArr[arrNr].pitchMoveStep,0.05);

            this.clipArr[arrNr].pitchMoveSingle = isNotNull(inValue.clipArr[arrNr].pitchMoveSActive,false);
            this.clipArr[arrNr].pitchMoveSingleIndex = 1.0;
            this.clipArr[arrNr].pitchMoveSingleDirection = isNotNull(inValue.clipArr[arrNr].pitchMoveSDirection,DIR.UP);
            this.clipArr[arrNr].pitchMoveSingleMax = isNotNull(inValue.clipArr[arrNr].pitchMoveSMax,DIR.UP);
            this.clipArr[arrNr].pitchMoveSingleDist = (Number(this.clipArr[arrNr].pitchMoveSingleMax)-Number(this.clipArr[arrNr].pitchMoveSingleIndex))/Number(this.chunkSize);
            
            this.clipArr[arrNr].volCalcSort = isNotNull(inValue.clipArr[arrNr].volCalcSort,CALC_VOL_TYPE.ADSL);
        
            this.clipArr[arrNr].adsr_Attack_StartVol = isNotNull(inValue.clipArr[arrNr].adsrAttackStartVol,0.00);
            this.clipArr[arrNr].adsr_Attack_StopVol = isNotNull(inValue.clipArr[arrNr].adsrAttackStopVol,1.00);
            this.clipArr[arrNr].adsr_Attack_StartNr = isNotNull(inValue.clipArr[arrNr].adsrAttackStartNr,0);
            this.clipArr[arrNr].adsr_Attack_StoptNr = isNotNull(inValue.clipArr[arrNr].adsrAttackStopNr,2000);
            this.clipArr[arrNr].adsr_Attack_VolStep = (Number(this.clipArr[arrNr].adsr_Attack_StopVol)-Number(this.clipArr[arrNr].adsr_Attack_StartVol)) / (Number(this.clipArr[arrNr].adsr_Attack_StoptNr)-Number(this.clipArr[arrNr].adsr_Attack_StartNr));
        
            this.clipArr[arrNr].adsr_Decay_StartVol = isNotNull(inValue.clipArr[arrNr].adsrDecayStartVol,1.00);
            this.clipArr[arrNr].adsr_Decay_StopVol = isNotNull(inValue.clipArr[arrNr].adsrDecayStopVol,0.60);
            this.clipArr[arrNr].adsr_Decay_StartNr = isNotNull(inValue.clipArr[arrNr].adsrDecayStartNr,2000);
            this.clipArr[arrNr].adsr_Decay_StoptNr = isNotNull(inValue.clipArr[arrNr].adsrDecayStopNr,5000);
            this.clipArr[arrNr].adsr_Decay_VolStep = (Number(this.clipArr[arrNr].adsr_Decay_StopVol)-Number(this.clipArr[arrNr].adsr_Decay_StartVol)) / (Number(this.clipArr[arrNr].adsr_Decay_StoptNr)-Number(this.clipArr[arrNr].adsr_Decay_StartNr));
        
            this.clipArr[arrNr].adsr_Sustain_StartVol = isNotNull(inValue.clipArr[arrNr].adsrSustainStartVol,0.60);
            this.clipArr[arrNr].adsr_Sustain_StopVol = isNotNull(inValue.clipArr[arrNr].adsrSustainStopVol,0.550);
            this.clipArr[arrNr].adsr_Sustain_StartNr = isNotNull(inValue.clipArr[arrNr].adsrSustainStartNr,5000);
            this.clipArr[arrNr].adsr_Sustain_StoptNr = isNotNull(inValue.clipArr[arrNr].adsrSustainStopNr,30000);
            this.clipArr[arrNr].adsr_Sustain_VolStep = (Number(this.clipArr[arrNr].adsr_Sustain_StopVol)-Number(this.clipArr[arrNr].adsr_Sustain_StartVol)) / (Number(this.clipArr[arrNr].adsr_Sustain_StoptNr)-Number(this.clipArr[arrNr].adsr_Sustain_StartNr));
        
            this.clipArr[arrNr].adsr_Release_StartVol = isNotNull(inValue.clipArr[arrNr].adsrReleaseStartVol,0.550);
            this.clipArr[arrNr].adsr_Release_StopVol = isNotNull(inValue.clipArr[arrNr].adsrReleaseStopVol,0.000);
            this.clipArr[arrNr].adsr_Release_StartNr = isNotNull(inValue.clipArr[arrNr].adsrReleaseStartNr,30000);
            this.clipArr[arrNr].adsr_Release_StoptNr = isNotNull(inValue.clipArr[arrNr].adsrReleaseStartNr,40000);
            this.clipArr[arrNr].adsr_Release_VolStep = (Number(this.clipArr[arrNr].adsr_Release_StopVol)-Number(this.clipArr[arrNr].adsr_Release_StartVol)) / (Number(this.clipArr[arrNr].adsr_Release_StoptNr)-Number(this.clipArr[arrNr].adsr_Release_StartNr));
        }

        this.nrOfClips = this.clipArr.length;
    }

    getChunk(buf0,buf1) {
        this.calcVolAndFreq();

        for(var j = 0;j<this.nrOfClips;j++) {
            var tempBuf0 = [];
            var tempBuf1 = [];
            for(var i = 0;i<this.chunkSize;i++) {
                tempBuf0[i]=0;
                tempBuf1[i]=0;
            }
            for(var i = 0;i<this.chunkSize;i++) {
                var obj =Waves.getSine(i,
                    this.clipArr[j].freqArr[i],Number(this.clipArr[j].volArr[i])* Number(this.clipArr[j].mainVol),
                    this.clipArr[j].cyklePos,tempBuf0,tempBuf1,
                    this.clipArr[j].pitch,this.sampleRate,
                    this.clipArr[j].lowPassFilterActive,this.sampleSize,
                    this.clipArr[j].balL,this.clipArr[j].balR
                ); 
                this.clipArr[j].cyklePos = obj.cyklePos;
            }
            mergeSnd(buf0,buf1,tempBuf0,tempBuf1);
        }
    }
    calcVolAndFreq() {
        for(var j = 0;j<this.nrOfClips;j++) {
            for(var i = 0;i<this.chunkSize;i++) {
                this.clipArr[j].freqArr[i] = this.clipArr[j].freq;

                if(i>= this.clipArr[j].adsr_Attack_StartNr && i<this.clipArr[j].adsr_Attack_StoptNr) {
                    this.clipArr[j].vol = Number(this.clipArr[j].vol) + Number(this.clipArr[j].adsr_Attack_VolStep);
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Decay_StartNr && i<this.clipArr[j].adsr_Decay_StoptNr) {
                    this.clipArr[j].vol = Number(this.clipArr[j].vol) +  Number(this.clipArr[j].adsr_Decay_VolStep);
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Sustain_StartNr && i<this.clipArr[j].adsr_Sustain_StoptNr) {
                    this.clipArr[j].vol = Number(this.clipArr[j].vol) +  Number(this.clipArr[j].adsr_Sustain_VolStep);
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Release_StartNr && i<this.clipArr[j].adsr_Release_StoptNr) {
                    this.clipArr[j].vol = Number(this.clipArr[j].vol) +  Number(this.clipArr[j].adsr_Release_VolStep);
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }

                if(this.clipArr[j].volMove) {
                    this.clipArr[j].volArr[i] =Number(this.clipArr[j].volArr[i]) + Number(this.clipArr[j].volMoveIndex);
                    if(this.clipArr[j].volMoveDirection == DIR.UP) {
                        this.clipArr[j].volMoveIndex = Number(this.clipArr[j].volMoveIndex) + Number(this.clipArr[j].volMoveDist);
                        if(this.clipArr[j].volMoveIndex > this.clipArr[j].volMoveMax) {
                            this.clipArr[j].volMoveDirection = DIR.DOWN;
                        }
                    }
                    else if(this.clipArr[j].volMoveDirection == DIR.DOWN) {
                        this.clipArr[j].volMoveIndex  = Number(this.clipArr[j].volMoveIndex) - Number(this.clipArr[j].volMoveDist);
                        if(this.clipArr[j].volMoveIndex < this.clipArr[j].volMoveMin) {
                            this.clipArr[j].volMoveDirection = DIR.UP;
                        }
                    }
                }

                if(this.clipArr[j].pitchMove) {
                    this.clipArr[j].freqArr[i] =Number(this.clipArr[j].freqArr[i]) + Number(this.clipArr[j].pitchMoveIndex);
                    if(this.clipArr[j].pitchMoveDirection == DIR.UP) {
                        this.clipArr[j].pitchMoveIndex  = Number(this.clipArr[j].pitchMoveIndex) + Number(this.clipArr[j].pitchMoveDist);
                        if(this.clipArr[j].pitchMoveIndex > this.clipArr[j].pitchMoveMax) {
                            this.clipArr[j].pitchMoveDirection = DIR.DOWN;
                        }
                    }
                    else if(this.clipArr[j].pitchMoveDirection == DIR.DOWN) {
                        this.clipArr[j].pitchMoveIndex  = Number(this.clipArr[j].pitchMoveIndex) - Number(this.clipArr[j].pitchMoveDist);
                        if(this.clipArr[j].pitchMoveIndex < this.clipArr[j].pitchMoveMin) {
                            this.clipArr[j].pitchMoveDirection = DIR.UP;
                        }
                    }
                }
                
                if(this.clipArr[j].pitchMoveSingle) {
                    this.clipArr[j].freqArr[i] =Number(this.clipArr[j].freqArr[i]) + Number(this.clipArr[j].pitchMoveSingleIndex);
                    if(this.clipArr[j].pitchMoveSingleDirection == DIR.UP) {
                        this.clipArr[j].pitchMoveSingleIndex = Number(this.clipArr[j].pitchMoveSingleIndex) + Number(this.clipArr[j].pitchMoveSingleDist);
                    }
                    else if(this.clipArr[j].pitchMoveSingleDirection == DIR.DOWN) {
                        this.clipArr[j].pitchMoveSingleIndex  = Number(this.clipArr[j].pitchMoveSingleIndex) - Number(this.clipArr[j].pitchMoveSingleDist);
                    }
                }



                if(this.clipArr[j].volArr[i] < 0.000) {
                    this.clipArr[j].volArr[i] = 0.000
                }
                this.clipArr[j].volArr[i] = Number(this.clipArr[j].volArr[i]).toFixed(7);

            }
        }
    }

} 