class SimpleSineWaveSirenMultiple extends AbstractSoundClip {
    clipArr = [];
    nrOfClips = -1;

	constructor() {
		super();
        this.chunkSize = 40000;
        this.init();
    }
    init() {
        var arrNr = -1;
//--
        this.clipArr.push({})
        arrNr++;

        this.clipArr[arrNr].vol = 0.0;
        this.clipArr[arrNr].balL = 1.0;
        this.clipArr[arrNr].balR = 1.0;
        this.clipArr[arrNr].freq = 1720.0;
        this.clipArr[arrNr].volArr = [];
        this.clipArr[arrNr].freqArr = [];
        this.clipArr[arrNr].cyklePos = 0;
        this.clipArr[arrNr].lowPassFilterActive = null;
        this.clipArr[arrNr].pitch = 1.0;

        this.clipArr[arrNr].volMove = false;
        this.clipArr[arrNr].volMoveIndex = 1.0;
        this.clipArr[arrNr].volMoveDirection = DIR.UP;
        this.clipArr[arrNr].volMoveMax = 25;
        this.clipArr[arrNr].volMoveMin = -this.clipArr[arrNr].volMoveMax;
        this.clipArr[arrNr].volMoveDist = 0.01;

        this.clipArr[arrNr].pitchMove = false;
        this.clipArr[arrNr].pitchMoveIndex = 1.0;
        this.clipArr[arrNr].pitchMoveDirection = DIR.UP;
        this.clipArr[arrNr].pitchMoveMax = 250;
        this.clipArr[arrNr].pitchMoveMin = -this.clipArr[arrNr].pitchMoveMax;
        this.clipArr[arrNr].pitchMoveDist = 0.05;

        this.clipArr[arrNr].pitchMoveSingle = true;
        this.clipArr[arrNr].pitchMoveSingleIndex = 1.0;
        this.clipArr[arrNr].pitchMoveSingleDirection = DIR.UP;
        this.clipArr[arrNr].pitchMoveSingleMax = 1000;
        this.clipArr[arrNr].pitchMoveSingleDist = (this.clipArr[arrNr].pitchMoveSingleMax-this.clipArr[arrNr].pitchMoveSingleIndex)/this.chunkSize;
        
    
        this.clipArr[arrNr].adsr_Attack_StartVol = 0.000;
        this.clipArr[arrNr].adsr_Attack_StopVol = 1.000;
        this.clipArr[arrNr].adsr_Attack_StartNr = 0;
        this.clipArr[arrNr].adsr_Attack_StoptNr = 2000;
        this.clipArr[arrNr].adsr_Attack_VolStep = (this.clipArr[arrNr].adsr_Attack_StopVol-this.clipArr[arrNr].adsr_Attack_StartVol) / (this.clipArr[arrNr].adsr_Attack_StoptNr-this.clipArr[arrNr].adsr_Attack_StartNr);
    
        this.clipArr[arrNr].adsr_Decay_StartVol = 1.000;
        this.clipArr[arrNr].adsr_Decay_StopVol = 0.600;
        this.clipArr[arrNr].adsr_Decay_StartNr = 2000;
        this.clipArr[arrNr].adsr_Decay_StoptNr = 5000;
        this.clipArr[arrNr].adsr_Decay_VolStep = (this.clipArr[arrNr].adsr_Decay_StopVol-this.clipArr[arrNr].adsr_Decay_StartVol) / (this.clipArr[arrNr].adsr_Decay_StoptNr-this.clipArr[arrNr].adsr_Decay_StartNr);
    
        this.clipArr[arrNr].adsr_Sustain_StartVol = 0.600;
        this.clipArr[arrNr].adsr_Sustain_StopVol = 0.550;
        this.clipArr[arrNr].adsr_Sustain_StartNr = 5000;
        this.clipArr[arrNr].adsr_Sustain_StoptNr = 30000;
        this.clipArr[arrNr].adsr_Sustain_VolStep = (this.clipArr[arrNr].adsr_Sustain_StopVol-this.clipArr[arrNr].adsr_Sustain_StartVol) / (this.clipArr[arrNr].adsr_Sustain_StoptNr-this.clipArr[arrNr].adsr_Sustain_StartNr);
    
        this.clipArr[arrNr].adsr_Release_StartVol = 0.550;
        this.clipArr[arrNr].adsr_Release_StopVol = 0.000;
        this.clipArr[arrNr].adsr_Release_StartNr = 30000;
        this.clipArr[arrNr].adsr_Release_StoptNr = 40000;
        this.clipArr[arrNr].adsr_Release_VolStep = (this.clipArr[arrNr].adsr_Release_StopVol-this.clipArr[arrNr].adsr_Release_StartVol) / (this.clipArr[arrNr].adsr_Release_StoptNr-this.clipArr[arrNr].adsr_Release_StartNr);
    //--

        this.clipArr.push({})
        arrNr++;

        this.clipArr[arrNr].vol = 0.0;
        this.clipArr[arrNr].balL = 1.0;
        this.clipArr[arrNr].balR = 1.0;
        this.clipArr[arrNr].freq = 880.0;
        this.clipArr[arrNr].volArr = [];
        this.clipArr[arrNr].freqArr = [];
        this.clipArr[arrNr].cyklePos = 0;
        this.clipArr[arrNr].lowPassFilterActive = null;
        this.clipArr[arrNr].pitch = 1.0;

        this.clipArr[arrNr].volMove = false;
        this.clipArr[arrNr].volMoveIndex = 1.0;
        this.clipArr[arrNr].volMoveDirection = DIR.UP;
        this.clipArr[arrNr].volMoveMax = 25;
        this.clipArr[arrNr].volMoveMin = -this.clipArr[arrNr].volMoveMax;
        this.clipArr[arrNr].volMoveDist = 0.01;

        this.clipArr[arrNr].pitchMove = false;
        this.clipArr[arrNr].pitchMoveIndex = 1.0;
        this.clipArr[arrNr].pitchMoveDirection = DIR.UP;
        this.clipArr[arrNr].pitchMoveMax = 250;
        this.clipArr[arrNr].pitchMoveMin = -this.clipArr[arrNr].pitchMoveMax;
        this.clipArr[arrNr].pitchMoveDist = 0.05;

        this.clipArr[arrNr].pitchMoveSingle = true;
        this.clipArr[arrNr].pitchMoveSingleIndex = 1.0;
        this.clipArr[arrNr].pitchMoveSingleDirection = DIR.UP;
        this.clipArr[arrNr].pitchMoveSingleMax = 1000;
        this.clipArr[arrNr].pitchMoveSingleDist = (this.clipArr[arrNr].pitchMoveSingleMax-this.clipArr[arrNr].pitchMoveSingleIndex)/this.chunkSize;
        
        this.clipArr[arrNr].adsr_Attack_StartVol = 0.000;
        this.clipArr[arrNr].adsr_Attack_StopVol = 1.000;
        this.clipArr[arrNr].adsr_Attack_StartNr = 0;
        this.clipArr[arrNr].adsr_Attack_StoptNr = 2000;
        this.clipArr[arrNr].adsr_Attack_VolStep = (this.clipArr[arrNr].adsr_Attack_StopVol-this.clipArr[arrNr].adsr_Attack_StartVol) / (this.clipArr[arrNr].adsr_Attack_StoptNr-this.clipArr[arrNr].adsr_Attack_StartNr);
    
        this.clipArr[arrNr].adsr_Decay_StartVol = 1.000;
        this.clipArr[arrNr].adsr_Decay_StopVol = 0.600;
        this.clipArr[arrNr].adsr_Decay_StartNr = 2000;
        this.clipArr[arrNr].adsr_Decay_StoptNr = 5000;
        this.clipArr[arrNr].adsr_Decay_VolStep = (this.clipArr[arrNr].adsr_Decay_StopVol-this.clipArr[arrNr].adsr_Decay_StartVol) / (this.clipArr[arrNr].adsr_Decay_StoptNr-this.clipArr[arrNr].adsr_Decay_StartNr);
    
        this.clipArr[arrNr].adsr_Sustain_StartVol = 0.600;
        this.clipArr[arrNr].adsr_Sustain_StopVol = 0.550;
        this.clipArr[arrNr].adsr_Sustain_StartNr = 5000;
        this.clipArr[arrNr].adsr_Sustain_StoptNr = 30000;
        this.clipArr[arrNr].adsr_Sustain_VolStep = (this.clipArr[arrNr].adsr_Sustain_StopVol-this.clipArr[arrNr].adsr_Sustain_StartVol) / (this.clipArr[arrNr].adsr_Sustain_StoptNr-this.clipArr[arrNr].adsr_Sustain_StartNr);
    
        this.clipArr[arrNr].adsr_Release_StartVol = 0.550;
        this.clipArr[arrNr].adsr_Release_StopVol = 0.000;
        this.clipArr[arrNr].adsr_Release_StartNr = 30000;
        this.clipArr[arrNr].adsr_Release_StoptNr = 40000;
        this.clipArr[arrNr].adsr_Release_VolStep = (this.clipArr[arrNr].adsr_Release_StopVol-this.clipArr[arrNr].adsr_Release_StartVol) / (this.clipArr[arrNr].adsr_Release_StoptNr-this.clipArr[arrNr].adsr_Release_StartNr);
    //--

        this.nrOfClips = this.clipArr.length;
    }

    getChunk(buf0,buf1) {
        this.calcVolAndFreq();

        for(var j = 0;j<this.nrOfClips;j++) {
            var tempBuf0 = [];
            var tempBuf1 = [];
            for(var i = 0;i<this.chunkSize;i++) {
                tempBuf0[i]=0;
                tempBuf1[i]=0;
            }
            for(var i = 0;i<this.chunkSize;i++) {
                var obj =Waves.getSine(i,
                    this.clipArr[j].freqArr[i],this.clipArr[j].volArr[i],
                    this.clipArr[j].cyklePos,tempBuf0,tempBuf1,
                    this.clipArr[j].pitch,this.sampleRate,
                    this.clipArr[j].lowPassFilterActive,this.sampleSize,
                    this.clipArr[j].balL,this.clipArr[j].balR
                ); 
                this.clipArr[j].cyklePos = obj.cyklePos;
            }
            mergeSnd(buf0,buf1,tempBuf0,tempBuf1);
        }
    }
    calcVolAndFreq() {
        for(var j = 0;j<this.nrOfClips;j++) {
            for(var i = 0;i<this.chunkSize;i++) {
                this.clipArr[j].freqArr[i] = this.clipArr[j].freq;

                if(i>= this.clipArr[j].adsr_Attack_StartNr && i<this.clipArr[j].adsr_Attack_StoptNr) {
                    this.clipArr[j].vol += this.clipArr[j].adsr_Attack_VolStep;
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Decay_StartNr && i<this.clipArr[j].adsr_Decay_StoptNr) {
                    this.clipArr[j].vol += this.clipArr[j].adsr_Decay_VolStep;
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Sustain_StartNr && i<this.clipArr[j].adsr_Sustain_StoptNr) {
                    this.clipArr[j].vol += this.clipArr[j].adsr_Sustain_VolStep;
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }
                else if(i>= this.clipArr[j].adsr_Release_StartNr && i<this.clipArr[j].adsr_Release_StoptNr) {
                    this.clipArr[j].vol += this.clipArr[j].adsr_Release_VolStep;
                    this.clipArr[j].volArr[i] = this.clipArr[j].vol;
                }

                if(this.clipArr[j].volMove) {
                    this.clipArr[j].volArr[i] =this.clipArr[j].volArr[i] + this.clipArr[j].volMoveIndex;
                    if(this.clipArr[j].volMoveDirection == DIR.UP) {
                        this.clipArr[j].volMoveIndex +=this.clipArr[j].volMoveDist;
                        if(this.clipArr[j].volMoveIndex > this.clipArr[j].volMoveMax) {
                            this.clipArr[j].volMoveDirection = DIR.DOWN;
                        }
                    }
                    else if(this.clipArr[j].volMoveDirection == DIR.DOWN) {
                        this.clipArr[j].volMoveIndex -=this.clipArr[j].volMoveDist;
                        if(this.clipArr[j].volMoveIndex < this.clipArr[j].volMoveMin) {
                            this.clipArr[j].volMoveDirection = DIR.UP;
                        }
                    }
                }

                if(this.clipArr[j].pitchMove) {
                    this.clipArr[j].freqArr[i] =this.clipArr[j].freqArr[i] + this.clipArr[j].pitchMoveIndex;
                    if(this.clipArr[j].pitchMoveDirection == DIR.UP) {
                        this.clipArr[j].pitchMoveIndex +=this.clipArr[j].pitchMoveDist;
                        if(this.clipArr[j].pitchMoveIndex > this.clipArr[j].pitchMoveMax) {
                            this.clipArr[j].pitchMoveDirection = DIR.DOWN;
                        }
                    }
                    else if(this.clipArr[j].pitchMoveDirection == DIR.DOWN) {
                        this.clipArr[j].pitchMoveIndex -=this.clipArr[j].pitchMoveDist;
                        if(this.clipArr[j].pitchMoveIndex < this.clipArr[j].pitchMoveMin) {
                            this.clipArr[j].pitchMoveDirection = DIR.UP;
                        }
                    }
                }
                
                if(this.clipArr[j].pitchMoveSingle) {
                    this.clipArr[j].freqArr[i] =this.clipArr[j].freqArr[i] + this.clipArr[j].pitchMoveSingleIndex;
                    if(this.clipArr[j].pitchMoveSingleDirection == DIR.UP) {
                        this.clipArr[j].pitchMoveSingleIndex +=this.clipArr[j].pitchMoveSingleDist;
                    }
                    else if(this.clipArr[j].pitchMoveSingleDirection == DIR.DOWN) {
                        this.clipArr[j].pitchMoveSingleIndex -=this.clipArr[j].pitchMoveSingleDist;
                    }
                }



                if(this.clipArr[j].volArr[i] < 0.000) {
                    this.clipArr[j].volArr[i] = 0.000
                }
                this.clipArr[j].volArr[i] = this.clipArr[j].volArr[i].toFixed(7);

            }
        }
    }

} 