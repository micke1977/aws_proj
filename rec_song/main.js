    
var tones = ['c','c+','d','d+','e','f','f+','g','g+','a','a+','b','c','c+','d','d+','e','f','f+','g','g+','a','a+','b','c','c+','d','d+','e','f','f+','g','g+','a','a+','b'];
var freqs = [261.6,	277.2,	293.7,	311.1,	329.6,	349.2,	370.0,	392.0,	415.3,	440.0,	466.2,	493.9]
var clickedBtns = [];
isRunning = false;
timerTime = 300;
steps = 256;
stepNr = 0;
nrOfOct = 3;
nrOfTones = nrOfOct*12;
pps = [];
context = new AudioContext()

function init() {
    element = document.querySelector('.mainContainer');
    element.style.display = 'block';
    element1 = document.querySelector('.initContainer');
    element1.style.visibility = 'hidden';

    var divElement = document.getElementById("scrollmenu")

    for(j=0;j<nrOfTones;j++) {
        var innerDivElement = document.createElement('div');
        if(j==0) {
            for(var i = 0;i<steps;i++) {
                const pp = document.createElement('input');
                pp.id = "p"+"_"+i;
                pp.disabled = true;
                pp.innerHTML = 'x'
                pp.className ='p_button';
                innerDivElement.appendChild(pp)
                pps.push(pp);
            }
        }
        for(var i = 0;i<steps;i++) {
            const button = document.createElement('button');
            button.innerText = '['+i+']  <'+tones[j]+'> ('+Math.trunc(j/12)+')';
            button.id = '['+i+']  <'+tones[j]+'> ('+Math.trunc(j/12)+')';
            if(i%8==0) {
                button.classList.add('yellowBorder')
            }
            button.addEventListener('click', () => {
                var selectedBtnToRemove = null;
                for(var b=0;b<clickedBtns.length;b++) {
                    if(clickedBtns[b].btnId==button.id) {
                        button.style.background="azure";
                        selectedBtnToRemove = b;
                        
                    } 
                }
                if(selectedBtnToRemove!=null) {
                    clickedBtns.splice(selectedBtnToRemove,1);
                } else {

                    var startOct = button.id.indexOf("(")
                    var stopOct = button.id.indexOf(")")
                    var subOct = button.id.substring(startOct+1,stopOct);

                    var startStep = button.id.indexOf("[")
                    var stopStep = button.id.indexOf("]")
                    var subStep = button.id.substring(startStep+1,stopStep);

                    var startTone = button.id.indexOf("<")
                    var stopTone = button.id.indexOf(">")
                    var subTone = button.id.substring(startTone+1,stopTone);

                    var toneNr = 0;
                    for(var i = 0;i<tones.length;i++) {
                        if(subTone==tones[i]) {
                            toneNr = i;
                            break;
                        }
                    }

                    clickedBtns.push({"btnId":button.id, "oct":subOct , "tone":subTone , "step":subStep, "toneNr":toneNr});
                    button.style.background="black";
                }
              })
            innerDivElement.appendChild(button)
        }
        divElement.appendChild(innerDivElement)
    }
    document.body.appendChild(divElement)

    document.getElementById("playButton").addEventListener('click', () => {
        isRunning = true;
        setTimeout(startFunction, timerTime)
    })
    
    document.getElementById("stopButton").addEventListener('click', () => {
        isRunning = false;
        stepNr = 0;
    })

    document.getElementById("printButton").addEventListener('click', () => {
        var txt = '';
        clickedBtns.forEach(b=> {
            txt += '\nstep='+b.step+' tone='+ b.tone+ " oct="+b.oct
        })
        alert(txt)
    })
}



function startFunction() {
    if(isRunning) {

        for(var i = 0;i<clickedBtns.length;i++) {
            if(clickedBtns[i].step == stepNr) {
                
                
                var frequency = freqs[clickedBtns[i].toneNr];
                if(clickedBtns[i].oct>0) {
                    for(var j=0;j<clickedBtns[i].oct;j++) {
                        frequency = frequency*2;
                    }
                }
                context.resume();
                o = context.createOscillator()
                g = context.createGain()
                o.connect(g)
                g.connect(context.destination)
                g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 3)
                o.frequency.value = parseFloat(frequency)
                o.start(0)
            }
        }
        for(var k=0;k<steps;k++) {
            if(k==stepNr) {
                pps[stepNr].className = 'p_button_sel'
            } else {
                pps[k].className = 'p_button'
            }
        }
    
        stepNr++
        if(stepNr>=steps) {
            stepNr=0;
        }

       
        setTimeout(startFunction, timerTime)
    }
}